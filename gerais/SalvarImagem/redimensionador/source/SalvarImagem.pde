// Need G4P library
import g4p_controls.*;
// You can remove the PeasyCam import if you are not using
// the GViewPeasyCam control or the PeasyCam library.
import peasy.*;
import java.io.*;

//PImage[] imagemSelecionada, imagemAlterada;
String parametroEditar, extensaoImg;
int numImgEditar, tamanhoFinal, tamanhoFinalPadrao;
File caminho;

public void setup()
{
  parametroEditar = "MAIOR";
  extensaoImg = ".png";
  tamanhoFinalPadrao = 940;
  tamanhoFinal = tamanhoFinalPadrao;
  size(600, 480, JAVA2D);
  createGUI();
  customGUI();
  // Place your setup code here
  //selectFolder("Escolha um arquivo", "pastaSelecionada");
}

public void draw()
{
  background(230); 
}

void PastaSelecionada(File selecao)
{
  if(selecao == null)
    println("Achou arquivo não, ou então tu apertou em Cancelar ó");
  else
  {
    infoTipoSelecao.setText("PASTA");
    infoCaminhoSelecao.setText(selecao.getAbsolutePath());
    //imagemSelecionada = selecao;
    caminho = selecao;
  }
}

String[] CarregaNomesArquivos(File selecao)
{
  FilenameFilter filtroNome = new FilenameFilter(){
    public boolean accept(File dir, String name)
    {
      return name.toLowerCase().endsWith(extensaoImg);
    }
  };
  return selecao.list(filtroNome);
}

void ArquivoSelecionado(File arquivo)
{
  if(arquivo == null)
    println("Achou arquivo não, ou então tu apertou em Cancelar ó");
  else
  {
    infoTipoSelecao.setText("IMAGEM");
    infoCaminhoSelecao.setText(arquivo.getAbsolutePath());
    caminho = arquivo;
  }
}

void ConverterImagem()
{
  boolean sucesso = false;
  if(infoTipoSelecao.getText() == "IMAGEM")
  {
    sucesso = Conversor(caminho.getAbsolutePath());
  }
  else if(infoTipoSelecao.getText() == "PASTA")
  {
    String[] listaImagens = CarregaNomesArquivos(caminho);
    String caminhoImagem = caminho.getAbsolutePath() + "\\";
    if(numImgEditar <= 0)
    {
      numImgEditar = listaImagens.length;
    }
    for(int i = 0; i < numImgEditar; i++)
    {
      sucesso = Conversor(caminhoImagem + listaImagens[i]);
    }
  }
  else
  {
    infoAviso.setText("Nenhum arquivo ou pasta foi selecionado. Operação cancelada");
  }
  
  if(sucesso)
    infoAviso.setText("Ação executada com sucesso");
  println(sucesso);
}

boolean Conversor(String caminhoImagem)
{
  PImage foto = loadImage(caminhoImagem);
  String[] nomeFoto = RecuperaNomeImagem(caminhoImagem);
  println("Eita: " + nomeFoto);
  
  if(parametroEditar == "LARGURA" || (parametroEditar == "MAIOR" && foto.width >= foto.height))
  {
    foto.resize(tamanhoFinal, 0);
  }
  else
  {
    foto.resize(0, tamanhoFinal);
  }
  
  return foto.save("\\Imagens Modificadas\\"+nomeFoto[0] + extensaoImg);
}

String[] RecuperaNomeImagem(String caminhoImagem)
{
  String[] nomeDividido = split(caminhoImagem, '\\');
  nomeDividido = split(nomeDividido[nomeDividido.length-1], '.');
  
  return nomeDividido;
}

// Use this method to add additional statements
// to customise the GUI controls
public void customGUI(){

}
