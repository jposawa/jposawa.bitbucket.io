//CONFIGURAÇÕES INICIAIS//
let detalhePagina, paginaAnterior, paginaAtual = "";
let extensaoUsada = "html";

let localFichas = "akFichas";

let valorInicialAtributo = 5;
let pontosIniciais = 20;
let custoAtributo = 2;
let custoTraco = 0;
let custoPericia = 1;

let usaVitae = false;

let fichaAberta = 0;

let secaoAtiva = 0;

//CONFIGURAÇÕES INICIAIS//

$(document).ready(function () {
    //PÁGINA INICIAL
	/*$("#corpo1").load("inicio.html");
	paginaAtual = "inicio.html";
    destacaPagina(paginaAtual.slice(0,paginaAtual.length-5)) //CHAMANDO FUNÇÃO COM O NOME RETIRANDO OS 5 CARACTERES FINAIS DA STRING (QUE NESSE CASO É ".HTML")
    */
    ConfiguracaoInicial();
    AlternaMenu();
    paginaAtual = "fichas";
    PageLoad(paginaAtual);
    //$("#cPrincipal").load(paginaAtual+"."+extensaoUsada);
    CarregaPagina();

    if (localStorage.getItem(localFichas) === null) {
        AtualizaLSComJSON("personagens", localFichas);
    }

    SoNumeros();
    //PreparaDivs();
    //RolagemPagina();
});

//ATUALIZA PARTES DO LOCALSTORAGE COM BASE EM ARQUIVOS
function AtualizaLSComJSON(nomeArquivo, nomeLS) {
    let arquivoJSON = nomeArquivo + ".json";
    $.getJSON(arquivoJSON, function (resultado) {
        let nvJSON = JSON.stringify(resultado);
        localStorage.setItem(nomeLS, nvJSON);
    });
}

function AtualizaLS(nomeLS, objeto) {
    let nvJSON = JSON.stringify(objeto);
    localStorage.setItem(nomeLS, nvJSON);
}

function ConfiguracaoInicial() {
    //$("#cPrincipal").css({"background":cor.secundariaClara,"color":cor.brancoPadrao});
    //$("#slotMenu").css({"background":cor.brancoPadrao, "color":cor.brancoPadrao});
    //$("#slotMenu p").css({"background": cor.secundariaEscura, "color":cor.primariaComplementar});
    //$("#menu li").css({"background": "", "color":cor.primaria});
    //$(".blocoPagina").css({"background": cor.brancoPadrao, "color": cor.primaria});
    //$(".menuPadrao").css({"background": "", "color": cor.primaria});
    //$(".selecionado").css({"background": cor.primaria, "color": cor.brancoPadrao});
}

function AlternaMenu() {
    let menuAberto = false;
    if (parseInt($(window).width()) < 950) {
        $("#slotMenu>p").html("<sup>&#9652;</sup>");

        $("#slotMenu>p").click(function () {
            console.log(menuAberto);
            if (!menuAberto) {
                $("#slotMenu>p sup").css("transform", "rotateX(180deg)");
                $("#slotMenu").css("height", "100%");
            }
            else {
                $("#slotMenu").css("height", "2.8rem");
                $("#slotMenu>p sup").css("transform", "rotateX(0deg)");
            }

            menuAberto = !menuAberto;
        });
    }
    else {
        $("#slotMenu>p").html($("title").html());
    }
}

function PreparaDivs() {
    let qtdDivs = $("#menu li").length;
    let txtHtml = "";

    for (let i = 0; i < qtdDivs; i++) {
        /*
        txtHtml += "<div id = \"secao";
        txtHtml += i;
        txtHtml += "\" class = \"blocoPagina\"></div>\n";
        */
        txtHtml += "<div class = \"blocoPagina\"></div>\n";
    }

    let corpoPrincipal = document.getElementById("cPrincipal");

    corpoPrincipal.innerHTML = txtHtml;
    GeraPaginas();
}


function GeraPaginas() {
    let listaPaginas = $("#menu li");
    let nomePagina = "";
    let blocosPaginas = $(".blocoPagina");

    for (let i = 0; i < listaPaginas.length; i++) {
        nomePagina = listaPaginas[i].attributes[0].value;
        nomePagina += ".html";
        //console.log(nomePagina);
        $(blocosPaginas[i]).load(nomePagina);
    }
}

function PageLoad(pagina) {
    detalhePagina = "";
    if (pagina != paginaAtual) {
        paginaAnterior = paginaAtual;
        paginaAtual = pagina;
    }

    $("#cPrincipal").load(paginaAtual + "." + extensaoUsada, function () {
        if (paginaAtual == "fichas")
            PegaFicha();
    });
}

function CarregaPagina() {
    $("#menu li").click(function () {
        let elementosMenu = $("#menu li");
        for (let i = 0; i < elementosMenu.length; i++) {
            //console.log(elementosMenu[i]);
            $(elementosMenu[i]).removeClass("selecionado");
        }
        PageLoad($(this).attr("data-page"));
        $(this).addClass("selecionado");
    });

    $(".detalhar").click(function () 
    {
        console.log("entrou no detalhamento");
        //CarregaDetalhe($(this).attr("data-detalhe"));
        /*paginaAnterior = paginaAtual;
        paginaAtual = $(this).attr("data-detalhe");


        if(paginaAtual.split("%")[0] === "ficha") //SIGNIFICA QUE VAI DETALHAR UMA FICHA
        {

        }
        */
    });

    $(".botaoRedondo").click(function () 
    {
        switch ($(this).attr("data-botao") == "voltar") 
        {
            case "voltar":
                PageLoad(paginaAnterior);
                break;

            case "voltarDetalhe":
                PageLoad(paginaAtual);
                break;
        }
    });
    //console.log("DEPOIS CArregar pagina\n"+$("#cPrincipal").html());
    /*$("#menu li").removeClass("selecionado");
    nomePagina += "." + extensaoUsada;
    $("#cPrincipal").load(nomePagina);*/
}

function Alerta() 
{
    console.log("ALEEEERTA");
}

function CarregaDetalhe(detalhar) 
{
    detalhePagina = detalhar;
    detalhar = detalhar.split("%");
    switch (detalhar[0]) //CHECAR QUAL TIPO DE DETALHAMENTO
    {
        case "ficha":
            PegaFicha(detalhar[1]);
            break;
    }
}

function CarregaFichas(personagem, listaTodos) {
    //console.log("entrou");
    //id = parseInt(id);
    //let personagem = [];
    //console.log(PegaFicha(id));
    //personagem.push(PegaFicha(id));
    //console.log(id+"\nRetorno personagem: "+PegaFicha(1));
    let conteudoFichas = "";
    //console.log(conteudoFichas);
    if (listaTodos) //SIGNIFICA QUE ESTÁ LISTANDO TODOS
    {
        if (personagem.length > 1) 
        {
            for (let i = 1; i < personagem.length; i++) 
            {
                conteudoFichas += "<div class=\"blocoPagina corPrincipalFundo corBrancoPadraoConteudo\" data-detalhe=\"ficha%" + personagem[i].id + "\"><div class=\"faixaSuperiorBloco\"><h3 class=\"destaqueTexto1\">";
                conteudoFichas += personagem[i].nome + "</h3>";
                conteudoFichas += "</div><div class=\"slotEsq1\">";

                conteudoFichas += "<p>" + personagem[i].resumo + "</p>";
                conteudoFichas += "</div><div class=\"slotEsq2\"><img class=\"charMiniFoto\" src=\"" + personagem[i].imagem + "\">";
                conteudoFichas += "<span><p>Mesa:</p><p class=\"corComplementarConteudo\">";
                conteudoFichas += personagem[i].aventura + "</p></span>";
                conteudoFichas += "</div><div class=\"slotAtributos\">";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">PV</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].pv + "</span></p>";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">PF</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].pf + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">ST</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].st + "</span></p>";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">DX</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].dx + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">IQ</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].iq + "</span></p>";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">HT</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].ht + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">PER</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].per + "</span></p>";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">BC</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].bc + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrGrande\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">DANO</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].dano + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<p class=\"acessoDetalhes corComplementarFundo corPrincipalConteudo\" onclick=\"PegaFicha(" + personagem[i].id + ")\">Mais informações</p>";

                conteudoFichas += "</div></div>";
            }
        }
        else 
        {
            conteudoFichas += "<div>NENHUMA FICHA ENCONTRADA</div>";
        }
        $("#cPrincipal div").html(conteudoFichas);
    }
    else //SIGNIFICA QUE SERÁ DETALHADO
    {
        fichaAberta = personagem.id;
        $(".charMiniFoto").attr("src", personagem.imagem);
        $(".nomePersonagem").html(personagem.nome);
        $(".mesaPersonagem").html(personagem.aventura);
        //$(".slotEsq1 p").html(personagem.resumo);

        $(".slotEsq2 .corComplementarConteudo").html(personagem.aventura);

        $(".atrPontos input").val(personagem.pontos);
        $(".atrSt input").val(personagem.st);
        $(".atrDx input").val(personagem.dx);
        $(".atrIq input").val(personagem.iq);
        $(".atrHt input").val(personagem.ht);
        $(".atrPer input").val(personagem.per);
        $(".atrBc input").val(personagem.bc);
        $(".atrDano").html(personagem.dano);
        $(".atrPf input").val(personagem.pf);
        $(".atrPf input").attr("max", personagem.pfmax);
        $(".atrPfMax span").html(personagem.pfmax);
        $(".atrPv input").val(personagem.pv);
        $(".atrPv input").attr("max", personagem.pvmax);
        $(".atrPvMax input").val(personagem.pvmax);
        $(".anotacoes textarea").val(personagem.anotacoes);

        ListarPericias();
    }
}

function RecuperaFichasLS(idFicha) 
{
    //let localFichas = "dkFichas";
    let objeto = JSON.parse(localStorage.getItem(localFichas));;
    console.log(idFicha);
    if(idFicha == undefined)
    {
        return objeto;
    }
    else
    {
        idFicha = parseInt(idFicha);
        if (isNaN(idFicha)) 
        {
            console.log("ID passado não foi numérico");
            return false;
        }
        else 
        {
            let idJson;
            let numeroFichasEncontradas = objeto.filter(f => f.id === idFicha).length;
            if (numeroFichasEncontradas == 0) 
            {
                console.log("Deu ruim, o ID passado não está dentro do cadastro");
            }
            else //EXISTE NO JSON
            {
                idJson = objeto.findIndex(f => f.id === idFicha);
                //console.log("O id json dele é " + idJson);

                return objeto[idJson];
            }
        }
    }
}

function ListarPericias()
{
    let ficha = RecuperaFichasLS(fichaAberta);
    let listaPericias = ficha.pericia;

    let textoPericias = "";

    for(let i = 1; i <= 5; i++)
    {
        let nomeAtributo = "";

        switch(i)
        {
            case 1:
                nomeAtributo = "st";
                //textoCompetencias += "<h5>FÍSICO</h5>";
                break;
            
            case 2:
                nomeAtributo = "dx";
                //textoCompetencias += "<h5>COORDENAÇÃO</h5>";
                break;
            
            case 3:
                nomeAtributo = "iq";
                //textoCompetencias += "<h5>INTELIGÊNCIA</h5>";
                break;
            
            case 4:
                nomeAtributo = "ht";
                //textoCompetencias += "<h5>ASTÚCIA</h5>";
                break;

            case 5:
                nomeAtributo = "per";
                //textoCompetencias += "<h5>VONTADE</h5>";
                break;
        }

        let atributoPericia = listaPericias.filter(f => f.atrVinculado == nomeAtributo);
        atributoPericia.sort(function(a,b){return b.nv - a.nv});
        for(let j = 0; j < atributoPericia.length; j++)
        {
            textoPericias += "<p>" + atributoPericia[j].nomeP;
            textoPericias += " <input type = \"number\" value = \"" + atributoPericia[j].nv + "\" onfocus=\"MoveCursorFim(this)\" onblur=\"AtualizaPericia(this,'" + atributoPericia[j].nomeP + "')\" min=1 max=20 />";
            textoPericias += " <input type=\"button\" onclick=\"ExcluiPericia('" + atributoPericia[j].nomeP + "')\" value=\"&#10005;\"/></p>";
        }
    }

    $(".blcPericia").html(textoPericias);
}

function ExcluiPericia(periciaExcluida)
{
    let txtConfirmacao = "Deseja excluir esta Perícia?";
    console.log("entrou");
    if(confirm(txtConfirmacao))
    {
        let ficha = RecuperaFichasLS(fichaAberta);
        let listaPericias = ficha.pericia;

        let indicePericiaExcluida = listaPericias.findIndex(f => f.nomeP == periciaExcluida);

        listaPericias.splice(indicePericiaExcluida,1);

        AtualizaFichaJSON(fichaAberta, "pericia", listaPericias);

        ListarPericias();
    }
}

function AdicionarPericia()
{
    //let txtBlocoPericia = $(".blcPericia").html();
    $(".addPericia").addClass("escondido");
    $(".novaPericia").removeClass("escondido");
}

function ConfirmaNovaPericia(adicionar)
{
    if(adicionar)
    {
        let ficha = RecuperaFichasLS(fichaAberta);
        let listaPericias = ficha.pericia;
        listaPericias.push(
            {
                atrVinculado: $(".novaPericia select").val(),
                nomeP: $(".novaPericia input[type=text]").val(),
                nv: $(".novaPericia input[type=number]").val()
            });
        
        AtualizaFichaJSON(fichaAberta, "pericia", listaPericias);
        ListarPericias();
    }
    else
    {
        $(".novaPericia input[type=text]").val("");
        $(".novaPericia input[type=number]").val(1);
    }
    $(".novaPericia").addClass("escondido");
    $(".addPericia").removeClass("escondido");
}

function AtualizaPericia(campo,nomePericia)
{
    CorrigeIntervalo(campo);

    let ficha = RecuperaFichasLS(fichaAberta);
    let listaPericias = ficha.pericia;

    let idPericia = listaPericias.findIndex(f => f.nomeP == nomePericia);

    listaPericias[idPericia].nv = campo.value;

    AtualizaFichaJSON(fichaAberta, "pericia", listaPericias);
    
}

function AtualizaAnotacoes(campo)
{
    //console.log("entrou\n" + campo.value);
    AtualizaFichaJSON(fichaAberta, "anotacoes", campo.value);
}

function PegaFicha(idFicha) 
{
    idFicha = parseInt(idFicha);
    //let objetoResultado;
    let fichaJSON = localStorage.getItem(localFichas);
    let resultado = JSON.parse(fichaJSON);

    if (isNaN(idFicha) || idFicha <= 0 || idFicha > resultado.length)
    {
        console.log("ID passado não é válido, listando todas as fichas...");
        console.log("Resultado: " + resultado);
        CarregaFichas(resultado, true);
    }
    else {
        console.log("Pegando ficha de id " + idFicha + " | Posição " + idFicha);
        console.log("Resultado: " + resultado[idFicha]);
        $("#cPrincipal").load("fichadetalhada." + extensaoUsada, function () {
            console.log("ENTROU CARELAO");
            if(fichaAberta != idFicha)
                secaoAtiva = 0;
            CarregaFichas(resultado[idFicha]);
            MudaSecao(secaoAtiva);
        });
    }
    //console.log(resultado[idFicha-1]);
    //objetoResultado = resultado;
}

function AtualizaFichaJSON(idFicha, campoFicha, novoValor) {
    idFicha = parseInt(idFicha);
    if (isNaN(idFicha)) {
        console.log("ID passado não foi numérico");
        return false;
    }
    else {
        let ficha = RecuperaFichasLS();
        let idJson;
        let numeroFichasEncontradas = ficha.filter(f => f.id === idFicha).length;
        if (numeroFichasEncontradas == 0) {
            console.log("Deu ruim, o ID passado não está dentro do cadastro");
        }
        else //EXISTE NO JSON
        {
            idJson = ficha.findIndex(f => f.id === idFicha);
            //console.log("O id json dele é " + idJson);   

            if (Array.isArray(campoFicha)) //CHECA SE SERÁ ALTERADO MAIS DE UM CAMPO
            {
                for (let i = 0; i < campoFicha.length; i++) {
                    ficha[idJson][campoFicha[i].toLowerCase()] = novoValor[i];
                    switch (campoFicha[i].toLowerCase()) 
                    {
                        case "st":
                            ficha[idJson].bc = Math.floor(Math.pow(ficha[idJson].st,2)/10) + ficha[idJson].modbc;
                            ficha[idJson].dano = CalculaDano(ficha[idJson].st);
                            ficha[idJson].pvmax = ficha[idJson].st + ficha[idJson].modpv;

                            $(".atrBc input").val(ficha[idJson].bc);
                            $(".atrDano").html(ficha[idJson].dano);
                            $(".atrPvMax input").val(ficha[idJson].pvmax);
                            break;
                        
                        case "bc":
                            ficha[idJson].modbc = ficha[idJson].bc - ficha[idJson].st;
                            break;

                        case "ht":
                            ficha[idJson].pfmax = ficha[idJson].ht + ficha[idJson].modpf;

                            $(".atrPfMax input").val(ficha[idJson].pfmax);
                            break;

                        case "pvmax":
                            ficha[idJson].modpv = ficha[idJson].pvmax - ficha[idJson].st;
                            break;

                        case "pfmax":
                            ficha[idJson].modpf = ficha[idJson].pfmax - ficha[idJson].ht;
                            break;

                        case "fis":
                            ficha[idJson].resmax = CalculaResiliencia([ficha[idJson].fis, ficha[idJson].von, ficha[idJson].modres]);
                            //ficha[idJson].resmax = ficha[idJson].fis + ficha[idJson].von + ficha[idJson].modres + 20;
                            ficha[idJson].vitmax = ficha[idJson].fis + ficha[idJson].modvit + 5;

                            $(".atrVitMax span").html(ficha[idJson].vitmax);
                            $(".atrVit input").attr("max", ficha[idJson].vitmax);
                            $(".atrResMax span").html(ficha[idJson].resmax);
                            $(".atrRes input").attr("max", ficha[idJson].resmax);
                            CorrigeIntervalo(".atrVit input");
                            CorrigeIntervalo(".atrRes input");
                            break;

                        case "von":
                            ficha[idJson].resmax = CalculaResiliencia([ficha[idJson].fis, ficha[idJson].von, ficha[idJson].modres]);

                            $(".atrResMax span").html(ficha[idJson].resmax);
                            $(".atrRes input").attr("max", ficha[idJson].resmax);
                            CorrigeIntervalo(".atrRes input");
                            break;
                    }
                }
            }
            else {
                ficha[idJson][campoFicha.toLowerCase()] = novoValor;

                switch (campoFicha.toLowerCase()) 
                {
                    case "st":
                        ficha[idJson].bc = Math.floor(Math.pow(ficha[idJson].st,2)/10) + ficha[idJson].modbc;
                        ficha[idJson].dano = CalculaDano(ficha[idJson].st);
                        ficha[idJson].pvmax = ficha[idJson].st + ficha[idJson].modpv;

                        $(".atrBc input").val(ficha[idJson].bc);
                        $(".atrDano").html(ficha[idJson].dano);
                        $(".atrPvMax input").val(ficha[idJson].pvmax);
                        break;
                    
                    case "bc":
                        ficha[idJson].modbc = ficha[idJson].bc - ficha[idJson].st;
                        break;

                    case "ht":
                        ficha[idJson].pfmax = ficha[idJson].ht + ficha[idJson].modpf;

                        $(".atrPfMax input").val(ficha[idJson].pfmax);
                        break;

                    case "pvmax":
                        ficha[idJson].modpv = ficha[idJson].pvmax - ficha[idJson].st;
                        break;

                    case "pfmax":
                        ficha[idJson].modpf = ficha[idJson].pfmax - ficha[idJson].ht;
                        break;

                    case "fis":
                        ficha[idJson].resmax = CalculaResiliencia([ficha[idJson].fis, ficha[idJson].von, ficha[idJson].modres]);
                        //ficha[idJson].resmax = ficha[idJson].fis + ficha[idJson].von + ficha[idJson].modres + 20;
                        ficha[idJson].vitmax = ficha[idJson].fis + ficha[idJson].modvit + 5;

                        $(".atrVitMax span").html(ficha[idJson].vitmax);
                        $(".atrVit input").attr("max", ficha[idJson].vitmax);
                        $(".atrResMax span").html(ficha[idJson].resmax);
                        $(".atrRes input").attr("max", ficha[idJson].resmax);
                        CorrigeIntervalo(".atrVit input");
                        CorrigeIntervalo(".atrRes input");
                        break;

                    case "von":
                        ficha[idJson].resmax = CalculaResiliencia([ficha[idJson].fis, ficha[idJson].von, ficha[idJson].modres]);

                        $(".atrResMax span").html(ficha[idJson].resmax);
                        $(".atrRes input").attr("max", ficha[idJson].resmax);
                        CorrigeIntervalo(".atrRes input");
                        break;
                }
            }
            AtualizaLS(localFichas, ficha);
        }
    }
}

function CalculaDano(st)
{
    st = parseInt(st);
    let dano, gdp, geb = "";

    if(st == 100)
    {
        gdp = "11d";
        geb = "13d";
    }
    else if(st >= 95)
    {
        gdp = "10d+2";
        geb = "12d+2";
    }
    else if(st >= 90)
    {
        gdp = "10d";
        geb = "12d";
    }
    else if(st >= 85)
    {
        gdp = "9d+2";
        geb = "11d+2";
    }
    else if(st >= 80)
    {
        gdp = "9d";
        geb = "11d";
    }
    else if(st >= 75)
    {
        gdp = "8d+2";
        geb = "10d+2";
    }
    else if(st >= 70)
    {
        gdp = "8d";
        geb = "10d";
    }
    else if(st >= 65)
    {
        gdp = "7d+1";
        geb = "9d+2";
    }
    else if(st >= 60)
    {
        gdp = "7d-1";
        geb = "9d";
    }
    else if(st >= 55)
    {
        gdp = "6d";
        geb = "8d+1";
    }
    else if(st >= 50)
    {
        gdp = "5d+2";
        geb = "8d-1";
    }
    else if(st >= 45)
    {
        gdp = "5d";
        geb = "7d+1";
    }
    else if(st >= 39)
    {
        gdp = "4d+1";
        geb = "7d-1";
    }
    else if(st >= 37)
    {
        gdp = "4d";
        geb = "6d+2";
    }
    else if(st >= 35)
    {
        gdp = "4d-1";
        geb = "6d+1";
    }
    else if(st >= 33)
    {
        gdp = "3d+2";
        geb = "6d";
    }
    else if(st >= 31)
    {
        gdp = "3d+1";
        geb = "6d-1";
    }
    else if(st >= 29)
    {
        gdp = "3d";
        geb = "5d+2";
    }
    else if(st >= 27)
    {
        gdp = "3d-1";
        geb = "5d+1";
    }
    else if(st >= 25)
    {
        gdp = "2d+2";
        geb = "5d-1";
        if(st == 26)
            geb = "5d";
    }
    else if(st >= 23)
    {
        gdp = "2d+1";
        geb = "4d+1";
        if(st == 24)
            geb = "4d+2";
    }
    else if(st >= 21)
    {
        gdp = "2d";
        geb = "4d-1";
        if(st == 22)
            geb = "4d";
    }
    else if(st >= 19)
    {
        gdp = "2d-1";
        geb = "3d+1";
        if(st == 20)
            geb = "3d+2";
    }
    else if(st >= 17)
    {
        gdp = "1d+2";
        geb = "3d-1";
        if(st == 18)
            geb = "3d";
    }
    else if(st >= 15)
    {
        gdp = "1d+1";
        geb = "2d+1";
        if(st == 16)
            geb = "2d+2";
    }
    else if(st >= 13)
    {
        gdp = "1d";
        geb = "2d-1";
        if(st == 14)
            geb = "2d";
    }
    else if(st >= 11)
    {
        gdp = "1d-2";
        geb = "1d+1";
        if(st == 12)
            geb = "1d+2";
    }
    else if(st >= 9)
    {
        gdp = "1d-2";
        geb = "1d-1";
        if(st == 10)
            geb = "1d";
    }
    else if(st >= 7)
    {
        gdp = "1d-3";
        geb = "1d-2";
    }
    else if(st >= 5)
    {
        gdp = "1d-4";
        geb = "1d-3";
    }
    else if(st >= 3)
    {
        gdp = "1d-5";
        geb = "1d-4";
    }
    else if(st >= 1)
    {
        gdp = "1d-6";
        geb = "1d-5";
    }

    dano = gdp + "/" + geb;
    
    return dano;
}

function AtualizaDadosFicha(entrada, campo) 
{
    //let nomePampo = ".atr" + campo[0].toUpperCase() + campo.slice(1) + " input";
    let intervaloAceito = [entrada.min, entrada.max];
    let valor = entrada.value;

    //console.log(entrada);
    valor = parseInt(valor);

    if(isNaN(valor))
        valor = entrada.value;

    //console.log(valor);

    if (intervaloAceito != undefined) 
    {
        //intervaloAceito = intervaloAceito.split("-");
        intervaloAceito[0] = parseInt(intervaloAceito[0]);
        intervaloAceito[1] = parseInt(intervaloAceito[1]);

        if (valor < intervaloAceito[0])
            valor = intervaloAceito[0];
        else if (valor > intervaloAceito[1])
            valor = intervaloAceito[1];

        $(entrada).val(valor);
    }

    AtualizaFichaJSON(fichaAberta, campo, valor);
}

function CorrigeIntervalo(campo) {
    let intervaloAceito = [$(campo).attr("min"), $(campo).attr("max")];
    let valor = $(campo).val();
    console.log(campo);
    console.log(valor);
    if (intervaloAceito != undefined) {
        //intervaloAceito = intervaloAceito.split("-");
        intervaloAceito[0] = parseInt(intervaloAceito[0]);
        intervaloAceito[1] = parseInt(intervaloAceito[1]);

        if (valor < intervaloAceito[0])
            valor = intervaloAceito[0];
        else if (valor > intervaloAceito[1])
            valor = intervaloAceito[1];

        $(campo).val(valor);
    }
}

function CalculaResiliencia(atributos) {
    let resultado = 20;

    for (let i = 0; i < atributos.length; i++) {
        resultado += parseInt(atributos[i]);
    }

    return resultado;
}

function RolagemPagina() {
    $("#menu li").click(function () {
        let paginas = $(".blocoPagina");
        //let alturaAlvo = (parseFloat($(paginas).css("height")) * $(this).index());
        //if($(this).index() > 0)
        //console.log("MARGEM" + $(paginas).css("margin-top"));
        //alturaAlvo += parseFloat($(paginas).css("margin-top"));
        //alturaAlvo += 34;
        /*
        console.log(cPrincipal.scrollTop);
        console.log($(paginas[$(this).index()]).offset().top);
        console.log($(this).index());
        */
        let distScroll = (parseFloat($(paginas[$(this).index()]).css("height")) + 17) * $(this).index();
        //cPrincipal.scrollBy(0, distScroll);
        $("#cPrincipal").animate({ scrollTop: distScroll }, '50');
        //cPrincipal.scrollTo(0, alturaAlvo);
        //console.log(cPrincipal.scrollTop);
    });

    if (parseInt($(window).width()) >= 950) {
        $("#slotMenu>p").click(function () {
            $("#cPrincipal").animate({ scrollTop: 0 }, '50');
        });
    }
    /*
    let paginas = $(".blocoPagina");
    $("#cPrincipal").animate(
        {
            scrollTop: $(paginas[$(this).index()]).offset().top
        }, 800
    );
    console.log($(this).index());
});
*/
    //alert("eita");
    //alert(link.index());
}

function MudaSecao(valor) 
{
    let divsCorpo = $(".slotEsq1>div");
    let seletores = $(".seletor");
    let numValor = parseInt(valor);

    if (valor[0] != undefined) 
    {
        secaoAtiva += numValor;

        if (secaoAtiva >= divsCorpo.length)
            secaoAtiva = 0;
        else if (secaoAtiva < 0)
            secaoAtiva = divsCorpo.length - 1;
    }
    else 
    {
        secaoAtiva = numValor;
    }

    for (let i = 0; i < divsCorpo.length; i++) 
    {
        $(divsCorpo[i]).removeClass("ativo");
        $(seletores[i]).removeClass("selecionado");
    }

    console.log(secaoAtiva);

    $(divsCorpo[secaoAtiva]).addClass("ativo");
    $(seletores[secaoAtiva]).addClass("selecionado");
}

function number1() {
    var n1 = parseInt(Math.random() * (6 - 1) + 1);
    //alert(n1);

    switch (n1) {
        case 1:
            document.getElementById('dado1').src = "dados/1.png";
            break;
        case 2:
            document.getElementById('dado1').src = "dados/2.jpg";
            break;
        case 3:
            document.getElementById('dado1').src = "dados/3.png";
            break;
        case 4:
            document.getElementById('dado1').src = "dados/4.png";
            break;
        case 5:
            document.getElementById('dado1').src = "dados/5.png";
            break;
        case 6:
            document.getElementById('dado1').src = "dados/6.png";
            break;

    }
    return n1;
}

function number2() {
    var n2 = parseInt(Math.random() * (6 - 1) + 1);
    //alert(n2);

    switch (n2) {
        case 1:
            document.getElementById('dado2').src = "dados/1.png";
            break;
        case 2:
            document.getElementById('dado2').src = "dados/2.jpg";
            break;
        case 3:
            document.getElementById('dado2').src = "dados/3.png";
            break;
        case 4:
            document.getElementById('dado2').src = "dados/4.png";
            break;
        case 5:
            document.getElementById('dado2').src = "dados/5.png";
            break;
        case 6:
            document.getElementById('dado2').src = "dados/6.png";
            break;

    }
    return n2;
}

function ChamaEditarNome(chamaEditar)
{
    let personagem = RecuperaFichasLS(fichaAberta);
    if(chamaEditar)
    {
        $(".nomePersonagem").html("<input class=\"campoNomePersonagem\" type=\"text\" placeholder=\"Nome Personagem\" value=\""+personagem.nome+"\" onfocus=\"MoveCursorFim(this)\"/>");
        $(".botoesCabecalho input").removeClass("escondido");
        $(".botoesCabecalho>input").addClass("escondido");
        $(".campoNomePersonagem").focus();
    }
    else
    {
        $(".nomePersonagem").html(personagem.nome);
        $(".botoesCabecalho input").addClass("escondido");
        $(".botoesCabecalho>input").removeClass("escondido");
    }
}

function ConfirmaNovoNome(confirma)
{
    if(confirma)
    {
        AtualizaFichaJSON(fichaAberta, "nome", $(".campoNomePersonagem").val());
    }
    ChamaEditarNome();
}

function MoveCursorFim(campo)
{
    console.log("Eita maluco");
    let valorAtual = campo.value;
    campo.value = "";
    campo.value = valorAtual;
}

function number3() {
    var n3 = parseInt(Math.random() * (6 - 1) + 1);
    //alert(n3);

    switch (n3) {
        case 1:
            document.getElementById('dado3').src = "dados/1.png";
            break;
        case 2:
            document.getElementById('dado3').src = "dados/2.jpg";
            break;
        case 3:
            document.getElementById('dado3').src = "dados/3.png";
            break;
        case 4:
            document.getElementById('dado3').src = "dados/4.png";
            break;
        case 5:
            document.getElementById('dado3').src = "dados/5.png";
            break;
        case 6:
            document.getElementById('dado3').src = "dados/6.png";
            break;
    }
    return n3;
}

function rolarDados() {
    var i1 = number1();
    var i2 = number2();
    var i3 = number3();
    var soma = parseInt(i1) + parseInt(i2) + parseInt(i3);
    document.getElementById("soma").innerHTML = parseInt(soma);
    if (document.getElementById("at1").checked == true) {
        if (soma <= 8) {
            document.getElementById("resultado").innerHTML = "sucesso";
        } else {
            if (soma > 8) {
                document.getElementById("resultado").innerHTML = "falha";
            }
        }
    }
    if (document.getElementById("at2").checked == true) {
        if (soma <= 9) {
            document.getElementById("resultado").innerHTML = "sucesso";
        } else {
            if (soma > 9) {
                document.getElementById("resultado").innerHTML = "falha";
            }
        }
    }
    if (document.getElementById("at3").checked == true) {
        if (soma <= 10) {
            document.getElementById("resultado").innerHTML = "sucesso";
        } else {
            if (soma > 10) {
                document.getElementById("resultado").innerHTML = "falha";
            }
        }
    }
}

function SoNumeros() {
    $('input[type="number"]').keypress(function (e) {
        var a = [];
        var k = e.which;

        for (i = 48; i < 58; i++)
            a.push(i);

        if (!(a.indexOf(k) >= 0))
            e.preventDefault();
    });
}

function ResetarFichas()
{
    if(confirm("Confirma resetar as fichas? Essa ação não poderá ser desfeita"))
        AtualizaLSComJSON("personagens", localFichas);
}