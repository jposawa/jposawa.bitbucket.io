-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 10-Maio-2019 às 07:07
-- Versão do servidor: 5.7.24
-- versão do PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rpgc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atributo`
--

DROP TABLE IF EXISTS `atributo`;
CREATE TABLE IF NOT EXISTS `atributo` (
  `id_atributo` int(16) NOT NULL AUTO_INCREMENT,
  `nome_atributo` varchar(16) NOT NULL,
  `descricao_atributo` varchar(256) NOT NULL,
  `valormax_atributo` int(16) NOT NULL,
  PRIMARY KEY (`id_atributo`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `atributo`
--

INSERT INTO `atributo` (`id_atributo`, `nome_atributo`, `descricao_atributo`, `valormax_atributo`) VALUES
(1, 'Novo atributo', 'Descreva aqui o atributo se quiser ;)', 10),
(2, 'ForÃ§a', 'Torna o personagem forte', 15),
(3, 'InteligÃªncia', 'O personagem usa a lÃ³gica a seu favor', 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `atributopersonagem`
--

DROP TABLE IF EXISTS `atributopersonagem`;
CREATE TABLE IF NOT EXISTS `atributopersonagem` (
  `id_atributopersonagem` int(16) NOT NULL AUTO_INCREMENT,
  `id_atributo` int(16) NOT NULL,
  `id_personagem` int(16) NOT NULL,
  `valor_atributo` int(16) NOT NULL,
  PRIMARY KEY (`id_atributopersonagem`),
  KEY `id_atributo` (`id_atributo`),
  KEY `id_personagem` (`id_personagem`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id_item` int(16) NOT NULL AUTO_INCREMENT,
  `nome_item` varchar(16) NOT NULL,
  `descricao_item` varchar(16) NOT NULL,
  `efeito_item` varchar(256) NOT NULL,
  PRIMARY KEY (`id_item`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mesa`
--

DROP TABLE IF EXISTS `mesa`;
CREATE TABLE IF NOT EXISTS `mesa` (
  `id_mesa` int(16) NOT NULL AUTO_INCREMENT,
  `codigo_mesa` varchar(16) NOT NULL,
  `descricao_mesa` varchar(256) NOT NULL,
  `nome_mesa` varchar(32) NOT NULL,
  `login_usuario` varchar(16) NOT NULL,
  PRIMARY KEY (`id_mesa`),
  KEY `login_usuario` (`login_usuario`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mesa`
--

INSERT INTO `mesa` (`id_mesa`, `codigo_mesa`, `descricao_mesa`, `nome_mesa`, `login_usuario`) VALUES
(18, '2F5D3E', 'Conta a historia da aventura da mesa', 'Nova aventura', 'vanessa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mesapersonagem`
--

DROP TABLE IF EXISTS `mesapersonagem`;
CREATE TABLE IF NOT EXISTS `mesapersonagem` (
  `id_mesapersonagem` int(16) NOT NULL AUTO_INCREMENT,
  `id_mesa` int(16) NOT NULL,
  `id_personagem` int(16) NOT NULL,
  PRIMARY KEY (`id_mesapersonagem`),
  KEY `id_mesa` (`id_mesa`),
  KEY `id_personagem` (`id_personagem`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mesapersonagem`
--

INSERT INTO `mesapersonagem` (`id_mesapersonagem`, `id_mesa`, `id_personagem`) VALUES
(1, 18, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pericia`
--

DROP TABLE IF EXISTS `pericia`;
CREATE TABLE IF NOT EXISTS `pericia` (
  `id_pericia` int(16) NOT NULL AUTO_INCREMENT,
  `nome_pericia` varchar(16) NOT NULL,
  `descricao_pericia` varchar(16) NOT NULL,
  `valor_pericia` int(16) NOT NULL,
  `valormax_pericia` int(16) NOT NULL,
  PRIMARY KEY (`id_pericia`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pericia`
--

INSERT INTO `pericia` (`id_pericia`, `nome_pericia`, `descricao_pericia`, `valor_pericia`, `valormax_pericia`) VALUES
(1, 'qweqw', 'weqwe', 4, 10),
(2, 'qweqw', 'weqwe', 4, 10),
(3, 'qwwe', 'weqwe', 7, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `periciapersonagem`
--

DROP TABLE IF EXISTS `periciapersonagem`;
CREATE TABLE IF NOT EXISTS `periciapersonagem` (
  `id_periciapersonagem` int(16) NOT NULL AUTO_INCREMENT,
  `id_pericia` int(16) NOT NULL,
  `id_personagem` int(16) NOT NULL,
  PRIMARY KEY (`id_periciapersonagem`),
  KEY `id_pericia` (`id_pericia`),
  KEY `id_personagem` (`id_personagem`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `personagem`
--

DROP TABLE IF EXISTS `personagem`;
CREATE TABLE IF NOT EXISTS `personagem` (
  `id_personagem` int(16) NOT NULL AUTO_INCREMENT,
  `jogador_personagem` varchar(32) NOT NULL,
  `nome` varchar(32) NOT NULL,
  `titulo` varchar(256) NOT NULL,
  `idade` int(16) NOT NULL,
  `altura` int(16) NOT NULL,
  `peso` int(16) NOT NULL,
  `genero` varchar(16) NOT NULL,
  `raca` varchar(16) NOT NULL,
  `aparencia` varchar(256) NOT NULL,
  `frase` varchar(256) NOT NULL,
  PRIMARY KEY (`id_personagem`),
  KEY `jogador_personagem` (`jogador_personagem`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `personagem`
--

INSERT INTO `personagem` (`id_personagem`, `jogador_personagem`, `nome`, `titulo`, `idade`, `altura`, `peso`, `genero`, `raca`, `aparencia`, `frase`) VALUES
(1, 'vanessa', 'Gigante', 'Das cavernas', 37, 2, 112, 'Masculino', 'Gigante', 'Alto', 'Madeiraaaaa'),
(2, 'vanessa', ' hsgdsa', 'gdsagd', 12, 12, 12, 'Masculino', 'chdu', 'sdihs', 'sha'),
(10, 'vanessa', ' hsgdsa', ' hsgdsa', 12, 12, 12, 'Masculino', 'tgthjm', 'hghgv', 'uhjghm');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `nome_usuario` varchar(16) NOT NULL,
  `login_usuario` varchar(16) NOT NULL,
  `senha_usuario` varchar(16) NOT NULL,
  `email_usuario` varchar(16) NOT NULL,
  PRIMARY KEY (`login_usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`nome_usuario`, `login_usuario`, `senha_usuario`, `email_usuario`) VALUES
('Vanessa', 'vanessa', 'ufc123', 'vanessa@rpg'),
('leticia', 'leticia', 'ufc123', 'leticia@hmail'),
('xandra', 'xanda', 'ufc123', 'xanda@rpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
