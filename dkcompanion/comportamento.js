//CONFIGURAÇÕES INICIAIS//
let detalhePagina, paginaAnterior, paginaAtual = "";
let extensaoUsada = "html";

let localFichas = "dkFichas";

let valorInicialAtributo = 5;
let pontosIniciais = 20;
let custoAtributo = 2;
let custoTraco = 0;
let custoCompetencia = 1;

let usaVitae = false;

let fichaAberta = 0;

let secaoAtiva = 1;

//CONFIGURAÇÕES INICIAIS//

$(document).ready(function () 
{
    //PÁGINA INICIAL
	/*$("#corpo1").load("inicio.html");
	paginaAtual = "inicio.html";
    destacaPagina(paginaAtual.slice(0,paginaAtual.length-5)) //CHAMANDO FUNÇÃO COM O NOME RETIRANDO OS 5 CARACTERES FINAIS DA STRING (QUE NESSE CASO É ".HTML")
    */
    ConfiguracaoInicial();
    AlternaMenu();
    paginaAtual = "fichas";
    PageLoad(paginaAtual);
    //$("#cPrincipal").load(paginaAtual+"."+extensaoUsada);
    CarregaPagina();

    if (localStorage.getItem(localFichas) === null) {
        AtualizaLSComJSON("personagens", localFichas);
    }

    SoNumeros();
    //PreparaDivs();
    //RolagemPagina();
});

//ATUALIZA PARTES DO LOCALSTORAGE COM BASE EM ARQUIVOS
function AtualizaLSComJSON(nomeArquivo, nomeLS) {
    let arquivoJSON = nomeArquivo + ".json";
    $.getJSON(arquivoJSON, function (resultado) {
        let nvJSON = JSON.stringify(resultado);
        localStorage.setItem(nomeLS, nvJSON);
    });
}

function AtualizaLS(nomeLS, objeto) 
{
    let nvJSON = JSON.stringify(objeto);
    localStorage.setItem(nomeLS, nvJSON);
}

function ConfiguracaoInicial() {
    //$("#cPrincipal").css({"background":cor.secundariaClara,"color":cor.brancoPadrao});
    //$("#slotMenu").css({"background":cor.brancoPadrao, "color":cor.brancoPadrao});
    //$("#slotMenu p").css({"background": cor.secundariaEscura, "color":cor.primariaComplementar});
    //$("#menu li").css({"background": "", "color":cor.primaria});
    //$(".blocoPagina").css({"background": cor.brancoPadrao, "color": cor.primaria});
    //$(".menuPadrao").css({"background": "", "color": cor.primaria});
    //$(".selecionado").css({"background": cor.primaria, "color": cor.brancoPadrao});
}

function AlternaMenu() {
    let menuAberto = false;
    if (parseInt($(window).width()) < 950) {
        $("#slotMenu>p").html("<sup>&#9652;</sup>");

        $("#slotMenu>p").click(function () {
            console.log(menuAberto);
            if (!menuAberto) {
                $("#slotMenu>p sup").css("transform", "rotateX(180deg)");
                $("#slotMenu").css("height", "100%");
            }
            else {
                $("#slotMenu").css("height", "2.8rem");
                $("#slotMenu>p sup").css("transform", "rotateX(0deg)");
            }

            menuAberto = !menuAberto;
        });
    }
    else {
        $("#slotMenu>p").html($("title").html());
    }
}

function PreparaDivs() {
    let qtdDivs = $("#menu li").length;
    let txtHtml = "";

    for (let i = 0; i < qtdDivs; i++) {
        /*
        txtHtml += "<div id = \"secao";
        txtHtml += i;
        txtHtml += "\" class = \"blocoPagina\"></div>\n";
        */
        txtHtml += "<div class = \"blocoPagina\"></div>\n";
    }

    let corpoPrincipal = document.getElementById("cPrincipal");

    corpoPrincipal.innerHTML = txtHtml;
    GeraPaginas();
}


function GeraPaginas() {
    let listaPaginas = $("#menu li");
    let nomePagina = "";
    let blocosPaginas = $(".blocoPagina");

    for (let i = 0; i < listaPaginas.length; i++) {
        nomePagina = listaPaginas[i].attributes[0].value;
        nomePagina += ".html";
        //console.log(nomePagina);
        $(blocosPaginas[i]).load(nomePagina);
    }
}

/*
function CarregaPagina()
{
    let pagina = "index." + extensaoUsada;
    console.log(pagina);
    $("#cPrincipal").load(pagina);
}*/

function PageLoad(pagina) 
{
    detalhePagina = "";
    if (pagina != paginaAtual) 
    {
        paginaAnterior = paginaAtual;
        paginaAtual = pagina;
    }

    $("#cPrincipal").load(paginaAtual + "." + extensaoUsada, function () 
    {
        if (paginaAtual == "fichas")
            PegaFicha();
    });
}

function CarregaPagina() 
{
    $("#menu li").click(function () 
    {
        let elementosMenu = $("#menu li");
        for (let i = 0; i < elementosMenu.length; i++) 
        {
            //console.log(elementosMenu[i]);
            $(elementosMenu[i]).removeClass("selecionado");
        }
        PageLoad($(this).attr("data-page"));
        $(this).addClass("selecionado");
    });

    $(".detalhar").click(function () 
    {
        console.log("entrou no detalhamento");
        //CarregaDetalhe($(this).attr("data-detalhe"));
        /*paginaAnterior = paginaAtual;
        paginaAtual = $(this).attr("data-detalhe");


        if(paginaAtual.split("%")[0] === "ficha") //SIGNIFICA QUE VAI DETALHAR UMA FICHA
        {

        }
        */
    });

    $(".botaoRedondo").click(function () {
        switch ($(this).attr("data-botao") == "voltar") {
            case "voltar":
                PageLoad(paginaAnterior);
                break;

            case "voltarDetalhe":
                PageLoad(paginaAtual);
                break;
        }
    });
    //console.log("DEPOIS CArregar pagina\n"+$("#cPrincipal").html());
    /*$("#menu li").removeClass("selecionado");
    nomePagina += "." + extensaoUsada;
    $("#cPrincipal").load(nomePagina);*/
}

function Alerta() {
    console.log("ALEEEERTA");
}

function CarregaDetalhe(detalhar) {
    detalhePagina = detalhar;
    detalhar = detalhar.split("%");
    switch (detalhar[0]) //CHECAR QUAL TIPO DE DETALHAMENTO
    {
        case "ficha":
            PegaFicha(detalhar[1]);
            break;
    }
}

function CarregaFichas(personagem, listaTodos) {
    //console.log("entrou");
    //id = parseInt(id);
    //let personagem = [];
    //console.log(PegaFicha(id));
    //personagem.push(PegaFicha(id));
    //console.log(id+"\nRetorno personagem: "+PegaFicha(1));
    let conteudoFichas = "";
    //console.log(conteudoFichas);
    if (listaTodos) //SIGNIFICA QUE ESTÁ LISTANDO TODOS
    {
        if (personagem.length > 1) 
        {
            for (let i = 1; i < personagem.length; i++) 
            {
                conteudoFichas += "<div class=\"blocoPagina corPrincipalFundo corBrancoPadraoConteudo\" data-detalhe=\"ficha%" + personagem[i].id + "\"><div class=\"faixaSuperiorBloco\"><h3 class=\"destaqueTexto1\">";
                conteudoFichas += personagem[i].nome +"</h3>";
                conteudoFichas += "</div><div class=\"slotEsq1\">";

                conteudoFichas += "<p>" + personagem[i].resumo + "</p>";
                conteudoFichas += "</div><div class=\"slotEsq2\"><img class=\"charMiniFoto\" src=\"" + personagem[i].imagem + "\">";
                conteudoFichas += "<span><p>Mesa:</p><p class=\"corComplementarConteudo\">";
                conteudoFichas += personagem[i].aventura + "</p></span>";
                conteudoFichas += "</div><div class=\"slotAtributos\">";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">FIS</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].fis + "</span></p>";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">COO</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].coo + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">INT</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].int + "</span></p>";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">AST</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].ast + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">VON</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].von + "</span></p>";
                conteudoFichas += "<p class=\"atrPadrao\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">PRE</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].pre + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrGrande\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">RES</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].res + " / " + personagem[i].resmax + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<div class=\"atrLinha\">";
                conteudoFichas += "<p class=\"atrGrande\"><span class=\"corSecundariaEscuraFundo corBrancoPadraoConteudo ldEsq\">VIT</span><span class=\"corBrancoPadraoFundo corPrincipalConteudo ldDir\">" + personagem[i].vit + " / " + personagem[i].vitmax + "</span></p>";
                conteudoFichas += "</div>";

                conteudoFichas += "<p class=\"acessoDetalhes corComplementarFundo corPrincipalConteudo\" onclick=\"PegaFicha(" + personagem[i].id + ")\">Mais informações</p>";

                conteudoFichas += "</div></div>";
            }
            conteudoFichas += "<div><input type=\"button\" value=\"+\" onclick= \"NovaFicha()\"/></div>";
        }
        else {
            conteudoFichas += "<div>NENHUMA FICHA ENCONTRADA</div>";
        }
        $("#cPrincipal div").html(conteudoFichas);
    }
    else //SIGNIFICA QUE SERÁ DETALHADO
    {
        fichaAberta = personagem.id;
        $(".charMiniFoto").attr("src", personagem.imagem);
        $(".nomePersonagem").html(personagem.nome);
        $(".mesaPersonagem").html(personagem.aventura);

        $(".slotEsq2 .corComplementarConteudo").html(personagem.aventura);

        $(".atrCoo input").val(personagem.coo);
        $(".atrFis input").val(personagem.fis);
        $(".atrInt input").val(personagem.int);
        $(".atrAst input").val(personagem.ast);
        $(".atrVon input").val(personagem.von);
        $(".atrPre input").val(personagem.pre);
        $(".atrRes input").val(personagem.res);
        $(".atrRes input").attr("max", personagem.resmax);
        $(".atrResMax").html("/ <span>" + personagem.resmax + "</span>");
        $(".atrVit input").val(personagem.vit);
        $(".atrVit input").attr("max", personagem.vitmax);
        $(".atrVitMax").html("/ <span>" + personagem.vitmax + "</span>");
        $(".anotacoes textarea").val(personagem.anotacoes);

        ListarCompetencias();
    }
}

function RecuperaFichasLS(idFicha) 
{
    //let localFichas = "dkFichas";
    let objeto = JSON.parse(localStorage.getItem(localFichas));;
    console.log(idFicha);
    if(idFicha == undefined)
    {
        return objeto;
    }
    else
    {
        idFicha = parseInt(idFicha);
        if (isNaN(idFicha)) 
        {
            console.log("ID passado não foi numérico");
            return false;
        }
        else 
        {
            let idJson;
            let numeroFichasEncontradas = objeto.filter(f => f.id === idFicha).length;
            if (numeroFichasEncontradas == 0) 
            {
                console.log("Deu ruim, o ID passado não está dentro do cadastro");
            }
            else //EXISTE NO JSON
            {
                idJson = objeto.findIndex(f => f.id === idFicha);
                //console.log("O id json dele é " + idJson);

                return objeto[idJson];
            }
        }
    }
}

function ListarCompetencias()
{
    let ficha = RecuperaFichasLS(fichaAberta);
    let listaCompetencias = ficha.competencia;

    let textoCompetencias = "";

    for(let i = 1; i <= 6; i++)
    {
        let nomeAtributo = "";

        switch(i)
        {
            case 1:
                nomeAtributo = "fis";
                textoCompetencias += "<h5>FÍSICO</h5>";
                break;
            
            case 2:
                nomeAtributo = "coo";
                textoCompetencias += "<h5>COORDENAÇÃO</h5>";
                break;
            
            case 3:
                nomeAtributo = "int";
                textoCompetencias += "<h5>INTELIGÊNCIA</h5>";
                break;
            
            case 4:
                nomeAtributo = "ast";
                textoCompetencias += "<h5>ASTÚCIA</h5>";
                break;

            case 5:
                nomeAtributo = "von";
                textoCompetencias += "<h5>VONTADE</h5>";
                break;
            
            case 6:
                nomeAtributo = "pre";
                textoCompetencias += "<h5>PRESENÇA</h5>";
                break;
        }

        let atributoCompetencia = listaCompetencias.filter(f => f.atrVinculado == nomeAtributo);
        atributoCompetencia.sort(function(a,b){return b.nv - a.nv;});

        for(let j = 0; j < atributoCompetencia.length; j++)
        {
            textoCompetencias += "<p>" + atributoCompetencia[j].nomeC;
            textoCompetencias += " <input type = \"number\" value = \"" + atributoCompetencia[j].nv + "\" onfocus=\"MoveCursorFim(this)\" onblur=\"AtualizaCompetencia(this,'" + atributoCompetencia[j].nomeC + "')\" min=1 max=20 />";
            textoCompetencias += " <input type=\"button\" onclick=\"ExcluiCompetencia('" + atributoCompetencia[j].nomeC + "')\" value=\"&#10005;\"/></p>";
        }
    }

    $(".blcCompetencia").html(textoCompetencias);
}

function ExcluiCompetencia(competenciaExcluida)
{
    let txtConfirmacao = "Deseja excluir esta Competência?";
    console.log("entrou");
    if(confirm(txtConfirmacao))
    {
        let ficha = RecuperaFichasLS(fichaAberta);
        let listaCompetencias = ficha.competencia;

        let indiceCompetenciaExcluida = listaCompetencias.findIndex(f => f.nomeC == competenciaExcluida);

        listaCompetencias.splice(indiceCompetenciaExcluida,1);

        AtualizaFichaJSON(fichaAberta, "competencia", listaCompetencias);

        //ficha.numAlteracoes++;
        ListarCompetencias();
    }
}

function AdicionarCompetencia()
{
    //let txtBlocoCompetencia = $(".blcCompetencia").html();
    $(".addCompetencia").addClass("escondido");
    $(".novaCompetencia").removeClass("escondido");
}

function ConfirmaNovaCompetencia(adicionar)
{
    if(adicionar)
    {
        let ficha = RecuperaFichasLS(fichaAberta);
        let listaCompetencias = ficha.competencia;
        listaCompetencias.push(
            {
                atrVinculado: $(".novaCompetencia select").val(),
                nomeC: $(".novaCompetencia input[type=text]").val(),
                nv: $(".novaCompetencia input[type=number]").val()
            });
        
        AtualizaFichaJSON(fichaAberta, "competencia", listaCompetencias);

        //ficha.numAlteracoes++;
        ListarCompetencias();
    }

    $(".novaCompetencia input[type=text]").val("");
    $(".novaCompetencia input[type=number]").val(1);
    $(".novaCompetencia").addClass("escondido");
    $(".addCompetencia").removeClass("escondido");
}

function AtualizaCompetencia(campo,nomeCompetencia)
{
    CorrigeIntervalo(campo);

    let ficha = RecuperaFichasLS(fichaAberta);
    let listaCompetencias = ficha.competencia;

    let idCompetencia = listaCompetencias.findIndex(f => f.nomeC == nomeCompetencia);

    if(listaCompetencias[idCompetencia].nv != campo.value)
    {
        listaCompetencias[idCompetencia].nv = campo.value;

        AtualizaFichaJSON(fichaAberta, "competencia", listaCompetencias);
        ListarCompetencias();
    }
    
}

function AtualizaAnotacoes(campo)
{
    //console.log("entrou\n" + campo.value);
    let ficha = RecuperaFichaLS(fichaAberta);

    if(ficha.anotacoes != campo.value)
        AtualizaFichaJSON(fichaAberta, "anotacoes", campo.value);
}

function PegaFicha(idFicha) 
{
    idFicha = parseInt(idFicha);
    //let objetoResultado;
    let fichaJSON = localStorage.getItem(localFichas);
    let resultado = JSON.parse(fichaJSON);

    if (isNaN(idFicha) || idFicha <= 0 || idFicha > resultado.length)
    {
        console.log("ID passado não é válido, listando todas as fichas...");
        console.log("Resultado: " + resultado);
        CarregaFichas(resultado, true);
    }
    else 
    {
        console.log("Pegando ficha de id " + idFicha + " | Posição " + idFicha);
        console.log("Resultado: " + resultado[idFicha]);
        $("#cPrincipal").load("fichadetalhada." + extensaoUsada, function () 
        {
            console.log("ENTROU CARELAO");
            if(fichaAberta != idFicha)
                secaoAtiva = 1;
            CarregaFichas(resultado[idFicha]);
            MudaSecao(secaoAtiva);
        });
    }
    //console.log(resultado[idFicha-1]);
    //objetoResultado = resultado;
}

function NovaFicha()
{
    let ficha = RecuperaFichasLS();
    let numFichas = ficha.length - 1;
    ficha.push(ficha[0]);
    ficha[numFichas+1].id = ficha.length + 1;
    
    AtualizaLS(localFichas, ficha);
    PegaFicha(ficha[numFichas+1].id);
}

function AtualizaFichaJSON(idFicha, campoFicha, novoValor) 
{
    idFicha = parseInt(idFicha);
    if (isNaN(idFicha)) 
    {
        console.log("ID passado não foi numérico");
        return false;
    }
    else 
    {
        let ficha = RecuperaFichasLS();
        let idJson;
        let numeroFichasEncontradas = ficha.filter(f => f.id === idFicha).length;
        if (numeroFichasEncontradas == 0) 
        {
            console.log("Deu ruim, o ID passado não está dentro do cadastro");
        }
        else //EXISTE NO JSON
        {
            idJson = ficha.findIndex(f => f.id === idFicha);
            //console.log("O id json dele é " + idJson);   

            if (Array.isArray(campoFicha)) //CHECA SE SERÁ ALTERADO MAIS DE UM CAMPO
            {
                for (let i = 0; i < campoFicha.length; i++) 
                {
                    ficha[idJson][campoFicha[i].toLowerCase()] = novoValor[i];
                    switch (campoFicha[i].toLowerCase()) 
                    {
                        case "fis":
                            ficha[idJson].resmax = CalculaResiliencia([ficha[idJson].fis, ficha[idJson].von, ficha[idJson].modres]);
                            //ficha[idJson].resmax = ficha[idJson].fis + ficha[idJson].von + ficha[idJson].modres + 20;
                            ficha[idJson].vitmax = ficha[idJson].fis + ficha[idJson].modvit + 5;

                            $(".atrVitMax span").html(ficha[idJson].vitmax);
                            $(".atrVit input").attr("max", ficha[idJson].vitmax);
                            $(".atrResMax span").html(ficha[idJson].resmax);
                            $(".atrRes input").attr("max", ficha[idJson].resmax);
                            CorrigeIntervalo(".atrVit input");
                            CorrigeIntervalo(".atrRes input");
                            break;

                        case "von":
                            ficha[idJson].resmax = CalculaResiliencia([ficha[idJson].fis, ficha[idJson].von, ficha[idJson].modres]);

                            $(".atrResMax span").html(ficha[idJson].resmax);
                            $(".atrRes input").attr("max", ficha[idJson].resmax);
                            CorrigeIntervalo(".atrRes input");
                            break;
                    }
                }
            }
            else 
            {
                ficha[idJson][campoFicha.toLowerCase()] = novoValor;

                switch (campoFicha.toLowerCase()) {
                    case "fis":
                        ficha[idJson].resmax = CalculaResiliencia([ficha[idJson].fis, ficha[idJson].von, ficha[idJson].modres]);
                        //ficha[idJson].resmax = ficha[idJson].fis + ficha[idJson].von + ficha[idJson].modres + 20;
                        ficha[idJson].vitmax = ficha[idJson].fis + ficha[idJson].modvit + 5;

                        $(".atrVitMax span").html(ficha[idJson].vitmax);
                        $(".atrVit input").attr("max", ficha[idJson].vitmax);
                        $(".atrResMax span").html(ficha[idJson].resmax);
                        $(".atrRes input").attr("max", ficha[idJson].resmax);
                        CorrigeIntervalo(".atrVit input");
                        CorrigeIntervalo(".atrRes input");
                        break;

                    case "von":
                        ficha[idJson].resmax = CalculaResiliencia([ficha[idJson].fis, ficha[idJson].von, ficha[idJson].modres]);

                        $(".atrResMax span").html(ficha[idJson].resmax);
                        $(".atrRes input").attr("max", ficha[idJson].resmax);
                        CorrigeIntervalo(".atrRes input");
                        break;
                }
            }
            ficha[idJson].numAlteracoes++;

            AtualizaLS(localFichas, ficha);
        }
    }
}

function AtualizaDadosFicha(entrada, campo) {
    //let nomeCampo = ".atr" + campo[0].toUpperCase() + campo.slice(1) + " input";
    let intervaloAceito = [entrada.min, entrada.max];
    let valor = entrada.value;

    valor = parseInt(valor);

    if (intervaloAceito != undefined) {
        //intervaloAceito = intervaloAceito.split("-");
        intervaloAceito[0] = parseInt(intervaloAceito[0]);
        intervaloAceito[1] = parseInt(intervaloAceito[1]);

        if (valor < intervaloAceito[0])
            valor = intervaloAceito[0];
        else if (valor > intervaloAceito[1])
            valor = intervaloAceito[1];

        $(entrada).val(valor);
    }

    AtualizaFichaJSON(fichaAberta, campo, valor);
}

function CorrigeIntervalo(campo) 
{
    let intervaloAceito = [$(campo).attr("min"), $(campo).attr("max")];
    let valor = $(campo).val();
    console.log(campo);
    console.log(valor);
    if (intervaloAceito != undefined) {
        //intervaloAceito = intervaloAceito.split("-");
        intervaloAceito[0] = parseInt(intervaloAceito[0]);
        intervaloAceito[1] = parseInt(intervaloAceito[1]);

        if (valor < intervaloAceito[0])
            valor = intervaloAceito[0];
        else if (valor > intervaloAceito[1])
            valor = intervaloAceito[1];

        $(campo).val(valor);
    }
}

function CalculaResiliencia(atributos) {
    let resultado = 20;

    for (let i = 0; i < atributos.length; i++) {
        resultado += parseInt(atributos[i]);
    }

    return resultado;
}

function RolagemPagina() {
    $("#menu li").click(function () {
        let paginas = $(".blocoPagina");
        //let alturaAlvo = (parseFloat($(paginas).css("height")) * $(this).index());
        //if($(this).index() > 0)
        //console.log("MARGEM" + $(paginas).css("margin-top"));
        //alturaAlvo += parseFloat($(paginas).css("margin-top"));
        //alturaAlvo += 34;
        /*
        console.log(cPrincipal.scrollTop);
        console.log($(paginas[$(this).index()]).offset().top);
        console.log($(this).index());
        */
        let distScroll = (parseFloat($(paginas[$(this).index()]).css("height")) + 17) * $(this).index();
        //cPrincipal.scrollBy(0, distScroll);
        $("#cPrincipal").animate({ scrollTop: distScroll }, '50');
        //cPrincipal.scrollTo(0, alturaAlvo);
        //console.log(cPrincipal.scrollTop);
    });

    if (parseInt($(window).width()) >= 950) {
        $("#slotMenu>p").click(function () {
            $("#cPrincipal").animate({ scrollTop: 0 }, '50');
        });
    }
    /*
    let paginas = $(".blocoPagina");
    $("#cPrincipal").animate(
        {
            scrollTop: $(paginas[$(this).index()]).offset().top
        }, 800
    );
    console.log($(this).index());
});
*/
    //alert("eita");
    //alert(link.index());
}

function MudaSecao(valor) {
    let divsCorpo = $(".slotEsq1>div");
    let seletores = $(".seletor");
    let numValor = parseInt(valor);

    if (valor[0] != undefined) {
        secaoAtiva += numValor;

        if (secaoAtiva >= divsCorpo.length)
            secaoAtiva = 0;
        else if (secaoAtiva < 0)
            secaoAtiva = divsCorpo.length;
    }
    else {
        secaoAtiva = numValor;
    }

    for (let i = 0; i < divsCorpo.length; i++) {
        $(divsCorpo[i]).removeClass("ativo");
        $(seletores[i]).removeClass("selecionado");
    }

    console.log(secaoAtiva);

    $(divsCorpo[secaoAtiva-1]).addClass("ativo");
    $(seletores[secaoAtiva-1]).addClass("selecionado");
}

function number1() {
    var n1 = parseInt(Math.random() * (6 - 1) + 1);
    //alert(n1);

    switch (n1) {
        case 1:
            document.getElementById('dado1').src = "dados/1.png";
            break;
        case 2:
            document.getElementById('dado1').src = "dados/2.jpg";
            break;
        case 3:
            document.getElementById('dado1').src = "dados/3.png";
            break;
        case 4:
            document.getElementById('dado1').src = "dados/4.png";
            break;
        case 5:
            document.getElementById('dado1').src = "dados/5.png";
            break;
        case 6:
            document.getElementById('dado1').src = "dados/6.png";
            break;

    }
    return n1;
}

function number2() {
    var n2 = parseInt(Math.random() * (6 - 1) + 1);
    //alert(n2);

    switch (n2) {
        case 1:
            document.getElementById('dado2').src = "dados/1.png";
            break;
        case 2:
            document.getElementById('dado2').src = "dados/2.jpg";
            break;
        case 3:
            document.getElementById('dado2').src = "dados/3.png";
            break;
        case 4:
            document.getElementById('dado2').src = "dados/4.png";
            break;
        case 5:
            document.getElementById('dado2').src = "dados/5.png";
            break;
        case 6:
            document.getElementById('dado2').src = "dados/6.png";
            break;

    }
    return n2;
}

function ChamaEditarNome(chamaEditar)
{
    let personagem = RecuperaFichasLS(fichaAberta);
    if(chamaEditar)
    {
        $(".nomePersonagem").html("<input class=\"campoNomePersonagem\" type=\"text\" placeholder=\"Nome Personagem\" value=\""+personagem.nome+"\" onfocus=\"MoveCursorFim(this)\"/>");
        $(".botoesCabecalho input").removeClass("escondido");
        $(".botoesCabecalho>input").addClass("escondido");
        $(".campoNomePersonagem").focus();
    }
    else
    {
        $(".nomePersonagem").html(personagem.nome);
        $(".botoesCabecalho input").addClass("escondido");
        $(".botoesCabecalho>input").removeClass("escondido");
    }
}

function ConfirmaNovoNome(confirma)
{
    if(confirma)
    {
        AtualizaFichaJSON(fichaAberta, "nome", $(".campoNomePersonagem").val());
    }
    ChamaEditarNome();
}

function MoveCursorFim(campo)
{
    console.log("Eita maluco");
    let valorAtual = campo.value;
    campo.value = "";
    campo.value = valorAtual;
}

function number3() {
    var n3 = parseInt(Math.random() * (6 - 1) + 1);
    //alert(n3);

    switch (n3) {
        case 1:
            document.getElementById('dado3').src = "dados/1.png";
            break;
        case 2:
            document.getElementById('dado3').src = "dados/2.jpg";
            break;
        case 3:
            document.getElementById('dado3').src = "dados/3.png";
            break;
        case 4:
            document.getElementById('dado3').src = "dados/4.png";
            break;
        case 5:
            document.getElementById('dado3').src = "dados/5.png";
            break;
        case 6:
            document.getElementById('dado3').src = "dados/6.png";
            break;
    }
    return n3;
}

function rolarDados() {
    var i1 = number1();
    var i2 = number2();
    var i3 = number3();
    var soma = parseInt(i1) + parseInt(i2) + parseInt(i3);
    document.getElementById("soma").innerHTML = parseInt(soma);
    if (document.getElementById("at1").checked == true) {
        if (soma <= 8) {
            document.getElementById("resultado").innerHTML = "sucesso";
        } else {
            if (soma > 8) {
                document.getElementById("resultado").innerHTML = "falha";
            }
        }
    }
    if (document.getElementById("at2").checked == true) {
        if (soma <= 9) {
            document.getElementById("resultado").innerHTML = "sucesso";
        } else {
            if (soma > 9) {
                document.getElementById("resultado").innerHTML = "falha";
            }
        }
    }
    if (document.getElementById("at3").checked == true) {
        if (soma <= 10) {
            document.getElementById("resultado").innerHTML = "sucesso";
        } else {
            if (soma > 10) {
                document.getElementById("resultado").innerHTML = "falha";
            }
        }
    }
}

function SoNumeros() {
    $('input[type="number"]').keypress(function (e) {
        var a = [];
        var k = e.which;

        for (i = 48; i < 58; i++)
            a.push(i);

        if (!(a.indexOf(k) >= 0))
            e.preventDefault();
    });
}

function ResetarFichas()
{
    if(confirm("Confirma resetar as fichas? Essa ação não poderá ser desfeita"))
        AtualizaLSComJSON("personagens", localFichas);
}