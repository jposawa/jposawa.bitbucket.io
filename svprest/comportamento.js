$(document).ready(function()
{
	//PÁGINA INICIAL
	/*$("#corpo1").load("inicio.html");
	paginaAtual = "inicio.html";
    destacaPagina(paginaAtual.slice(0,paginaAtual.length-5)) //CHAMANDO FUNÇÃO COM O NOME RETIRANDO OS 5 CARACTERES FINAIS DA STRING (QUE NESSE CASO É ".HTML")
    */
    ConfiguracaoInicial();
    AlternaMenu();
    PreparaDivs();
    RolagemPagina();
})

function ConfiguracaoInicial()
{
    let cor = 
    {
        principal:"#"
    };
}

function AlternaMenu()
{
    let menuAberto = false;
    if(parseInt($(window).width()) < 950)
    {
        $("#slotMenu>p").html("<sup>&#9652;</sup>");
    
        $("#slotMenu>p").click(function()
        {
            console.log(menuAberto);
            if(!menuAberto)
            {
                $("#slotMenu>p sup").css("transform", "rotateX(180deg)");
                $("#slotMenu").css("height", "100%");
            }
            else
            {
                $("#slotMenu").css("height", "2.8rem");
                $("#slotMenu>p sup").css("transform", "rotateX(0deg)");
            }

            menuAberto = !menuAberto;
        });
    }
    else
    {
        $("#slotMenu>p").html("ServPrest");
    }
}

function PreparaDivs()
{
    let qtdDivs = $("#menu li").length;
    let txtHtml = "";

    for(let i = 0; i < qtdDivs; i++)
    {
        /*
        txtHtml += "<div id = \"secao";
        txtHtml += i;
        txtHtml += "\" class = \"blocoPagina\"></div>\n";
        */
        txtHtml += "<div class = \"blocoPagina\"></div>\n";
    }

    let corpoPrincipal = document.getElementById("cPrincipal");

    corpoPrincipal.innerHTML = txtHtml;
    GeraPaginas();
}

function GeraPaginas()
{
    let listaPaginas = $("#menu li");
    let nomePagina = "";
    let blocosPaginas = $(".blocoPagina");
    
    for(let i = 0; i < listaPaginas.length; i++)
    {
        nomePagina = listaPaginas[i].attributes[0].value;
        nomePagina += ".html";
        //console.log(nomePagina);
        $(blocosPaginas[i]).load(nomePagina);
    }
}

function RolagemPagina()
{
    $("#menu li").click(function()
    {
        let paginas = $(".blocoPagina");
        //let alturaAlvo = (parseFloat($(paginas).css("height")) * $(this).index());
        //if($(this).index() > 0)
            //console.log("MARGEM" + $(paginas).css("margin-top"));
            //alturaAlvo += parseFloat($(paginas).css("margin-top"));
            //alturaAlvo += 34;
        /*
        console.log(cPrincipal.scrollTop);
        console.log($(paginas[$(this).index()]).offset().top);
        console.log($(this).index());
        */
        let distScroll = (parseFloat($(paginas[$(this).index()]).css("height")) + 17) * $(this).index();
        //cPrincipal.scrollBy(0, distScroll);
        $("#cPrincipal").animate({scrollTop: distScroll}, '50');
        //cPrincipal.scrollTo(0, alturaAlvo);
        //console.log(cPrincipal.scrollTop);
    });

    if(parseInt($(window).width()) >= 950)
    {
        $("#slotMenu>p").click(function()
        {
            $("#cPrincipal").animate({scrollTop: 0}, '50');
        });
    }
        /*
        let paginas = $(".blocoPagina");
        $("#cPrincipal").animate(
            {
                scrollTop: $(paginas[$(this).index()]).offset().top
            }, 800
        );
        console.log($(this).index());
    });
    */
    //alert("eita");
    //alert(link.index());
}