//CONFIGURAÇÕES INICIAIS//

let vh = window.innerHeight * 0.01;

document.documentElement.style.setProperty('--vh', `${vh}px`);


let detalhePagina, paginaAnterior, paginaAtual = "";
let extensaoUsada = "html";

let local = 
{
    pessoa:"wPessoa"
}

let infos = 
{
    proxId:2,
    pessoa:""
}

//CONFIGURAÇÕES INICIAIS//

$(document).ready(function () 
{
    //PÁGINA INICIAL
    if(Cookies.get("wPessoa") == undefined)
        CarregaPagina("inicio");
    else
        CarregaPagina("pessoas",true);

    RecuperaInfos("wPessoa");
    //$("#cPrincipal").load(paginaAtual+"."+extensaoUsada);

    //if (localStorage.getItem(localFichas) === null) {}
});

function CalculaProxId()
{
    infos.proxId = infos.pessoa[infos.pessoa.length-1].id + 1;
}

function CarregaPagina(pagina, protegido, extensao)
{
    if(extensao == "" || extensao === null || extensao == undefined)
        extensao = extensaoUsada;
    
    //console.log(pagina + "\n" + extensao);
    if(protegido == true && Cookies.get("wPessoa") == undefined)
    {
        //Aviso("")
        alert("É necessário fazer login para acessar essa página");
        $("#cPrincipal").load("login.html");
    }
    else
    {
        $("#cPrincipal").load(pagina + "." + extensao, function()
        {
            $(".campoLogin").load("login.html");
        });
    }
}


//RETORNA MEU JSON
function RecuperaInfos(arqJson)
{
    console.log("entrou");
    arqJson = "json/" + arqJson + ".json";
    console.log(arqJson);

    $.getJSON(arqJson, function(resultado)
    {
        //console.log("entrou2");
        infos.pessoa = resultado;
        //console.log(resultado);
        CalculaProxId();
    });
}

function EncontraIndiceItem(info, idAlvo)
{
    let numInfoEncontradas = info.filter(f => f.id == idAlvo).length;

    if(numInfoEncontradas == 0)
    {
        alert("Esse id não existe");
        return false;
    }
    else
    {
        let indiceAlvo = info.findIndex(f => f.id === idAlvo);
        return indiceAlvo;
    }
}

function AddInfo(novaInfo)
{
    novaInfo.id = infos.proxId++;
    SalvaJSON("wPessoa", novaInfo);
}

function AlteraInfo(idAlvo, novaInfo)
{
    idAlvo = EncontraIndiceItem(infos.pessoa, idAlvo);
    infos.pessoa.splice(idAlvo, 1, novaInfo);
    SalvaJSON("wPessoa", infos.pessoa);
}

function DeletaInfo(idAlvo)
{
    idAlvo = EncontraIndiceItem(infos.pessoa, idAlvo);
    infos.pessoa.splice(idAlvo,1);
    SalvaJSON("wPessoa", infos.pessoa);
}

function SalvaJSON(nomeJson, objeto)
{
    nomeJson = "json/" + nomeJson + ".json";
    let stringJSON = {nome:nomeJson,conteudo:JSON.stringify(objeto)};
    console.log("obj: " + stringJSON);
    $.ajax(
    {
        type:"POST",
        url:"funcoes.php",
        data:stringJSON,
        async: false,
        success: function(resposta)
        {
            console.log(resposta);
        }
    });
}

function Aviso(texto)
{}