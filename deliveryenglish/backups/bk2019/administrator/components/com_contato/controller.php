<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class ContatoController extends JController
{
    public function display(){        
        $db = JFactory::getDbo();        
        $query = "SELECT * FROM #__contato WHERE tipo = 'contato'";
        $db->setQuery($query);
//        $contato = $db->loadObject();
//        $query = "SELECT * FROM #__contato WHERE tipo = 'setor'";
//        $db->setQuery($query);        
        $setor = $db->loadObjectList();
        $query = "SELECT * FROM #__contato WHERE tipo = 'mapa'";
        $db->setQuery($query);        
        $mapa = $db->loadObject();           
        $view = $this->getView('contato', 'html');
        $view->assignRef('mapa', $mapa);
        $view->assignRef('contato', $contato);
//        $view->assignRef('setor', $setor);
        $view->display();
    }
    
    
    public function apply(){        
            $nomec = $_POST['nomec'];
            $value = JRequest::getVar('value', '', 'post', 'string', JREQUEST_ALLOWRAW);           
            $db = JFactory::getDbo();            
            $db->setQuery("DELETE FROM #__contato WHERE `tipo` LIKE 'setor' OR `tipo` LIKE 'contato' OR `tipo` LIKE 'mapa'");
            $db->query();        
            $query = 'INSERT INTO #__contato (`id`, `nome`, `value`, `tipo`) VALUES';
            $query .= " (NULL, '{$nomec}','{$value}','contato')";
            $nomes = $_POST['nomes'];
            $emails = $_POST['emails'];

            foreach($nomes as $k => $nome){
                if($nome != ''){
                    $query .= ", (NULL, '{$nome}','{$emails[$k]}','setor')";
                }                            
            }
            if($_POST['mapa']){
                $query .= ", (NULL, 'mapa','{$_POST['mapa']}','mapa')";
            }
            $query .= ';';        
            $db->setQuery($query);
            $db->query($query);        

            if($db->getErrorMsg()){
                $this->setRedirect('index.php?option=com_contato', $db->getErrorMsg(), 'error');            
            }
            else{
                $this->setRedirect('index.php?option=com_contato', "Salvo com Sucesso");
            }                                   
        }    
}
?>
