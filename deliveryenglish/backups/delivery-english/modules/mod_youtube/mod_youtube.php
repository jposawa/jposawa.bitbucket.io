<?php
defined('_JEXEC') or die('Restricted access');
require_once dirname(__FILE__).'/helper.php';
$channel = $params->get('channel');
$item = ModYoutubeHelper::getYoutubeCurl($channel, null, true);
$code = ModYoutubeHelper::getCodeVideo($item->item[0]->guid);
$code1 = ModYoutubeHelper::getCodeVideo($item->item[1]->guid);
$code2 = ModYoutubeHelper::getCodeVideo($item->item[2]->guid);
require JModuleHelper::getLayoutPath('mod_youtube', $params->get('layout', 'default'));
