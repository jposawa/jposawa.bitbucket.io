<?php

defined('_JEXEC') or die('Restricted access');

class ModYoutubeHelper
{
    
    public function getYoutube($chanel, $tags='', $all=false)
    {
	$chanel = strtolower($chanel);
	if( $tags ){
	    $tags = '&category='.$tags;
	}
	$link = "http://gdata.youtube.com/feeds/base/users/{$chanel}/uploads?alt=rss&v=2&orderby=published&client=ytapi-youtube-profile{$tags}";
	$data = file_get_contents($link);
	$return = new SimpleXmlElement($data, LIBXML_NOCDATA);
	if($all){
	    return $return;
	}
	return $return->channel;    
    }

    public function getYoutubeCurl($chanel, $tags='', $all=false)
    {
	$chanel = strtolower($chanel);
	if( $tags ){
	    $tags = '&category='.$tags;
	}
	$link = "http://gdata.youtube.com/feeds/base/users/{$chanel}/uploads?alt=rss&v=2&orderby=published&client=ytapi-youtube-profile{$tags}";
	$ch = curl_init($link);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
	$data = curl_exec($ch);
	curl_close($ch);
	$return = new SimpleXmlElement($data, LIBXML_NOCDATA);
	if($all){
	    return $return;
	}
	return $return->channel;
    }
    
    public function getYoutubeFopen()
    {
	$chanel = strtolower($chanel);
	if( $tags ){
	    $tags = '&category='.$tags;
	}
	$link = "http://gdata.youtube.com/feeds/base/users/{$chanel}/uploads?alt=rss&v=2&orderby=published&client=ytapi-youtube-profile{$tags}";
	$resource = fopen($link);
	$data = stream_get_contents($resource);
	$fclose($resource);
	$return = new SimpleXmlElement($data, LIBXML_NOCDATA);
	if($all){
	    return $return;
	}
	return $return->channel; 
    }
    
    public function getCodeVideo($guid)
    {
	$codeVideo = explode(':', $guid);
	$count = count($codeVideo);
	return $codeVideo[$count-1];
    }
}
