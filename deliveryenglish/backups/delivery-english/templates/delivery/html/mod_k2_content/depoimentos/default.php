<?php
/**
 * @version		$Id: default.php 1499 2012-02-28 10:28:38Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<ul>
<?php foreach ($items as $key=>$item):	?>
   <li>
       <blockquote>
           <p>
               <?php echo strip_tags($item->introtext); ?>
               <span><?php echo $item->title; ?>, <?php echo $item->extra_fields[0]->value; ?></span>
           </p>
       </blockquote>

   </li>
<?php endforeach; ?>
</ul>
<div id="navDepoimentos"></div>