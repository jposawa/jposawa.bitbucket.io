<?php
/**
 * @version		$Id: default.php 1499 2012-02-28 10:28:38Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="box noticias">
    <div class="titulo">
        <h2>Notícias</h2>
    </div>
    <ul>
        <?php foreach ($items as $key=>$item):	?>
        <li>
            <a href="<?php echo $item->link; ?>" title="<?php echo $item->title; ?>">
                <small><?php echo JHTML::_('date', $item->created, 'd'); ?><span class="acessibilidade">de</span><br /><?php echo JHTML::_('date', $item->created, 'M'); ?></small>
                <?php echo $item->title; ?>
            </a>
        </li>
        <?php endforeach; ?>
        <p><a href="<?php echo $params->get('itemCustomLinkURL'); ?>" class="todas" title="<?php echo $itemCustomLinkTitle; ?>"><?php echo $itemCustomLinkTitle; ?></a></p>
    </ul>
</div>