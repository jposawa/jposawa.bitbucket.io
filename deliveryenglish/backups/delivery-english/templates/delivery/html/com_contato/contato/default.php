<?php defined('_JEXEC') or die;
$params = JComponentHelper::getParams('com_contato');
$input = JFactory::getApplication()->input;
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="<?php echo JURI::base();?>templates/delivery/js/jgmap.js" type="text/javascript"></script>
<script type="text/javascript">    
    jQuery(document).ready(function(){   
        jQuery('#enviar').click(function(){     
            email = jQuery('#email').val();
            mensagem = 'Preencher ';
            count = 0;
            if(jQuery('#nome').val() == ''){
                mensagem = mensagem + 'Nome';
                count = 1
            }
            if(email.search(/^\w+((-\w+)|(\.\w+))*\@\w+((\.|-)\w+)*\.\w+$/) == -1){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }                
                mensagem = mensagem +  'E-mail';
                count = 1                
            }
            if(jQuery('#telefone').val() == ''){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }
                mensagem = mensagem +   'Telefone';
                count = 1
            }
            if(jQuery('#mensagem').val() == ''){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }                
                mensagem = mensagem +   'Mensagem';
                count = 1                
            }       
            
            if(mensagem != 'Preencher '){
                alert(mensagem);
                return false;
            }
            
        })
        jQuery('#formContato').ajaxForm({
            success:       showResponse
        });
        <?php if($this->params->get('endereco') != ''): ?>
            jQuery("#mapa").gMap({ markers: [{ address: "<?php echo $this->params->get('endereco'); ?>" }],
                                    zoom: 15});
        <?php endif; ?>
    });   
    function showResponse(response){
        alert(response);
        jQuery('#formContato').clearForm();
    }
</script> 

    <div class="f-left">
        <div class="titulo">
            <h2>Entre em contato. Será um prazer atendê-lo.</h2>
        </div>
        <div class="texto">
            <form action="index.php?option=com_contato&task=enviar&Itemid=<?php echo $input->get('Itemid'); ?>" method="post" id="formContato">
                <fieldset>
                    <p class="span">

                        <label for="nome">Nome *:</label>

                        <input type="text" class="input-text" id="nome" name="nome" />

                    </p>

                    <p class="span">

                        <label for="email">E-mail* :</label>

                        <input type="text" class="input-text"  id="email" name="email" />

                    </p>

                    <p class="span ultimo">

                        <label for="telefone">Telefone *:</label>

                        <input type="text" class="input-text"  id="telefone" name="telefone" maxlength="8" />

                    </p>

                    <div class="clear" ></div>

                    <p>

                        <label for="mensagem">Mensagem* :</label>

                        <textarea id="mensagem" class="input-text" name="mensagem"></textarea>

                    </p>

                    <p>
                        <input type="submit" id="enviar" title="Enviar Mensagem" class="btn f-right" value="Enviar Mensagem">
                    </p>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="lateral">
        <div class="titulo">
            <h2>ligue ou visite-nos.</h2>
        </div>
        <div class="mapa">
            <?php if($this->params->get('endereco') != ''): ?>
            <div class="preview" style="height: 166px; width: 248px;" id="mapa"></div>
            <?php endif; ?>
            <address>
                <?php echo nl2br($this->params->get('texto')); ?>
            </address>
            <span>
                <span class="tel">+55 (85) <strong>9696.5400</strong> </span>
            </span>
        </div>
    </div>
