<?php defined('_JEXEC') or die;
$params = JComponentHelper::getParams('com_contato');
$input = JFactory::getApplication()->input;
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="<?php echo JURI::base();?>templates/delivery/js/jgmap.js" type="text/javascript"></script>
<script type="text/javascript">    
    jQuery(document).ready(function(){   
        jQuery('#enviar').click(function(){     
            email = jQuery('#email').val();
            mensagem = 'Preencher ';
            count = 0;
            if(jQuery('#nome').val() == ''){
                mensagem = mensagem + 'Nome';
                count = 1
            }
            if(email.search(/^\w+((-\w+)|(\.\w+))*\@\w+((\.|-)\w+)*\.\w+$/) == -1){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }                
                mensagem = mensagem +  'E-mail';
                count = 1                
            }
            if(jQuery('#telefone').val() == ''){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }
                mensagem = mensagem +   'Telefone';
                count = 1
            }
            
            if(mensagem != 'Preencher '){
                alert(mensagem);
                return false;
            }
            
        })
        jQuery('#formContato').ajaxForm({
            success:       showResponse
        });
        <?php if($this->params->get('endereco') != ''): ?>
            jQuery("#mapa").gMap({ markers: [{ address: "<?php echo $this->params->get('endereco'); ?>" }],
                                    zoom: 15});
        <?php endif; ?>
    });   
    function showResponse(response){
        alert(response);
        if(response == 'Enviado com sucesso'){
            jQuery('#formContato').clearForm();
        }
    }
</script> 

    <div class="f-left" style="width: 100%">
        <div class="titulo">
            <h2>Trabalhe Conosco</h2>
        </div>
        <div class="texto">
            <form action="index.php?option=com_contato&task=enviarTrabalhe&Itemid=<?php echo $input->get('Itemid'); ?>" method="post" id="formContato" enctype="multipart/form-data">
                <fieldset>
                    <p class="span" style="width: 295px">

                        <label for="nome">Nome *:</label>

                        <input type="text" class="input-text" id="nome" name="nome" style="width: 285px" />

                    </p>

                    <p class="span" style="width: 295px">

                        <label for="email">E-mail* :</label>

                        <input type="text" class="input-text"  id="email" name="email" style="width: 285px"/>

                    </p>

                    <p class="span ultimo" style="width: 295px">

                        <label for="telefone">Telefone *:</label>

                        <input type="text" class="input-text"  id="telefone" name="telefone" maxlength="8" style="width: 285px" />

                    </p>

                    <div class="clear" ></div>

                    <p>

                        <label for="file">Curriculum* :</label>

                        <input type="file" name="file" id="file" />

                    </p>

                    <p>
                        <input type="submit" id="enviar" title="Enviar" class="btn f-right" value="Enviar">
                    </p>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="lateral">
     <!--  <div class="titulo">
            <h2>ligue ou visite-nos.</h2>
        </div>
        <div class="mapa">
            <div class="preview" style="height: 166px; width: 248px;" id="mapa"></div>
            <address>
                <?php echo nl2br($this->params->get('texto')); ?>
            </address>
            <span>
                <span class="tel">+55 (85) <strong>9696.5400</strong> </span>               
            </span>
        </div> -->
    </div>
