<?php // no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<h2>Vídeos</h2>
<ul>
<?php $count = 0 ?>
<?php foreach($item->channel->item as $video){ if($count == 3){break; }?>
    <?php if($count == 0){ ?>
        <li>
            <iframe height="200" frameborder="0" width="350" allowfullscreen="" src="http://www.youtube.com/embed/<?php echo ModYoutubeHelper::getCodeVideo($video->guid); ?>"></iframe>
            <span><?php echo $video->title ?></span>
        </li>
    <?php }else{ ?>
    <li>
        <a title="<?php echo $video->title ?>" href="http://www.youtube.com/watch?v=<?php echo ModYoutubeHelper::getCodeVideo($video); ?>">
            <span><?php echo $video->title ?></span>
        </a>
    </li>
<?php } $count ++; } ?>
</ul>
<p>
    <a title="Ir para o Canal no Youtube" class="todas" href="http://www.youtube.com.br/<?php echo $channel; ?>">Canal no Youtube</a>
</p>