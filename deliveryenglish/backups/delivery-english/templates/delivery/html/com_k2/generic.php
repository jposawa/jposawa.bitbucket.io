<?php
/**
 * @version		$Id: generic.php 1492 2012-02-22 17:40:09Z joomlaworks@gmail.com $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<ul class="listagem">
    <div class="titulo">
        <h2><?php echo $this->escape($this->params->get('page_title')); ?></h2>
    </div>
    <?php if(count($this->items)): ?>
        <?php foreach($this->items as $item): ?>
        <li>
            <small class="data"><?php echo JHTML::_('date', $item->created , 'd'); ?><span class="acessibilidade">de</span> <?php echo JHTML::_('date', $item->created , 'M'); ?></small>
            <a title="<?php echo $item->title; ?>" href="<?php echo $item->link; ?>">
                <?php echo $item->title; ?>
            </a>
            <a title="Saiba Mais" class="todas" href="<?php echo $this->item->link; ?>">Saiba Mais</a>
        </li>
        <?php endforeach; ?>
    <?php else: ?>
        <li class="texto"><p>Nenhum item encontrado</p></li>
    <?php endif; ?>
</ul>
<?php if(count($this->pagination->getPagesLinks())): ?>
<?php if($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>
<?php endif; ?>