<?php
/**
 * @version		$Id: category_item.php 1492 2012-02-22 17:40:09Z joomlaworks@gmail.com $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>
<small class="data"><?php echo JHTML::_('date', $this->item->created , 'd'); ?><span class="acessibilidade">de</span> <?php echo JHTML::_('date', $this->item->created , 'M'); ?></small>
<a title="<?php echo $this->item->title; ?>" href="<?php echo $this->item->link; ?>">
    <?php echo $this->item->title; ?>
</a>
<a title="Saiba Mais" class="todas" href="<?php echo $this->item->link; ?>">Saiba Mais</a>