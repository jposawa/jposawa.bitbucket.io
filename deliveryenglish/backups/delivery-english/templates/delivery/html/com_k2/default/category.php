<?php
/**
 * @version		$Id: category.php 1527 2012-03-12 12:45:31Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<ul class="listagem">
    <div class="titulo">
        <h2><?php echo $this->category->name; ?></h2>
    </div>
    <?php if((isset($this->leading) || isset($this->primary) || isset($this->secondary) || isset($this->links)) && (count($this->leading) || count($this->primary) || count($this->secondary) || count($this->links))): ?>
        <?php if(isset($this->leading) && count($this->leading)): ?>
            <?php foreach($this->leading as $key=>$item): ?>
                <li>
                    <?php
                    $this->item=$item;
                    echo $this->loadTemplate('item');
                    ?>
                </li>
            <?php endforeach; ?>
        <?php else: ?>
            <li><h3>Nenhum item encontrado</h3></li>
        <?php endif; ?>
    <?php endif; ?>
</ul>
<?php if(count($this->pagination->getPagesLinks())): ?>
        <?php if($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>
<?php endif; ?>
