<?php
defined('_JEXEC') or die('Restricted access');
?>



<div class="titulo">

    <span><?php echo $this->item->category->name; ?></span>
    <h2><?php echo $this->item->title; ?></h2>

</div>

<div class="texto">

    <?php if(!empty($this->item->fulltext)): ?>
    <?php if($this->item->params->get('itemIntroText')): ?>
        <!-- Item introtext -->
            <?php echo $this->item->introtext; ?>
        <?php endif; ?>
    <?php if($this->item->params->get('itemFullText')): ?>
        <!-- Item fulltext -->
            <?php echo $this->item->fulltext; ?>
        <?php endif; ?>
    <?php else: ?>
    <!-- Item text -->
        <?php echo $this->item->introtext; ?>
    <?php endif; ?>

</div>
<?php if(JRequest::getInt('print')!=1): ?>
<div class="social">

    <strong>Compartilhe:</strong>

    <ul>
        <?php if($this->item->params->get('itemTwitterButton',1)): ?>
        <!-- Twitter Button -->
        <li class="itemTwitterButton">
            <a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal"<?php if($this->item->params->get('twitterUsername')): ?> data-via="<?php echo $this->item->params->get('twitterUsername'); ?>"<?php endif; ?>><?php echo JText::_('K2_TWEET'); ?></a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
        </li>
        <?php endif; ?>

        <?php if($this->item->params->get('itemFacebookButton',1)): ?>
        <!-- Facebook Button -->
        <li class="itemFacebookButton">
            <div id="fb-root"></div>
            <script type="text/javascript">
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/all.js#appId=177111755694317&xfbml=1";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            </script>
            <div class="fb-like" data-send="false" data-width="200" data-show-faces="true"></div>
        </li>
        <?php endif; ?>

        <?php if($this->item->params->get('itemGooglePlusOneButton',1)): ?>
        <!-- Google +1 Button -->
        <li class="itemGooglePlusOneButton">
            <g:plusone annotation="inline" width="120"></g:plusone>
            <script type="text/javascript">
                (function() {
                    window.___gcfg = {lang: 'en'}; // Define button default language here
                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                    po.src = 'https://apis.google.com/js/plusone.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                })();
            </script>
        </li>
        <?php endif; ?>
    </ul>

</div>
<?php endif; ?>