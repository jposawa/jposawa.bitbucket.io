
jQuery(document).ready(function() {
   
    jQuery('.depoimentos ul').cycle({
		fx: 'fade',
		height: "210px",
		pager:  '#navDepoimentos'
	});
	
	jQuery('.slideshow ul').cycle({
		fx: 'fade',
		height: "390px",
		pager:  '#controle'
	});
	
});

function alert(mensagem, titulo, type){
    if(type == null){
        type = 'information';
    }
    if(titulo == null){
        titulo = 'Aviso';
    }
    jQuery.Zebra_Dialog(mensagem, {
        'type':     type,
        'title':    titulo
    });
}
