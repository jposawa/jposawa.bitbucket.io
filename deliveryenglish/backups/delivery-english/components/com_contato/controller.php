<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class ContatoController extends JController
{               
    public function enviar(){
        jimport( 'joomla.utilities.utility' );
        if($_POST['nome'] == ''){
            echo "Preencha campo nome";
            die;
        }
        if($_POST['email'] == ''){
            echo "Preencha campo e-mail";
            die;
        }
        if($_POST['telefone'] == ''){
            echo "Preencha campo telefone";
            die;
        }
        if($_POST['mensagem'] == ''){
            echo "Preencha campo mensagem";
            die;
        }
        $app = JFactory::getApplication();
        $params   = $app->getParams();            
        $config =& JFactory::getConfig();
        if($params->get('para') == ''):
            $para = $config->getValue( 'config.mailfrom' );
        else:
            $para = $params->get('para');
        endif;      
        $title = "Formulario Contato - Site - Delivery";
        $msg = "Nome: {$_POST['nome']}
            
E-mail: {$_POST['email']}

Telefone: {$_POST['telefone']}
                                
Msg: {$_POST['mensagem']}";
        $cc = $params->get('cc');
        if($cc == ''){            
            $cc = null;
        }else{
            $cc = explode(',', $cc);        
        }        
        $cco = $params->get('cco');
        if($cco == ''){            
            $cco = null;
        }else{
            $cco = explode(',', $cco);        
        }
        if(JUtility::sendMail($_POST['email'], $_POST['nome'], $para, $title, $msg,  0, $cc, $cco, null, $_POST['email'], $_POST['nome']))
        {
                echo "Enviado com Sucesso";        
        }else{
                echo "Falha ao Enviar";
        }
        die;
    }

    public function enviarTrabalhe(){
        $input = JFactory::getApplication()->input;
        if($input->get('Itemid') == ''){
            die('Sem identificador');
        }
        if($input->get('nome', '', 'string') == ''){
           die('Preencher campo nome');
        }
        if($input->get('telefone', '', 'string') == ''){
            die('Preencher campo telefone');
        }
        if(!filter_var($input->get('email', '', 'string'), FILTER_VALIDATE_EMAIL)){
            die('Preencher campo e-mail');
        }

        jimport('joomla.filesystem.file');
        $file = $_FILES['file'];
        if($file['name'] == ''){
            die('Nenhum arquivo enviado');
        }else{
            if(strtolower(JFile::getExt($file['name'])) == 'pdf' || strtolower(JFile::getExt($file['name'])) == 'docx' || strtolower(JFile::getExt($file['name'])) == 'doc'){
                $name = rand(1000, 9999).JFile::makeSafe($file['name']);
                $dest = JPATH_ROOT.DS.'media'.DS.'curriculos'.DS.$name;
                if(!JFile::upload($file['tmp_name'], $dest)){
                    die('Falha ao mover o arquivo');
                }else{
                    $app = JFactory::getApplication();
                    $params   = $app->getParams();
                    $config =& JFactory::getConfig();
                    $mail = JFactory::getMailer();
                    if($params->get('para') == ''):
                        $para = $config->getValue( 'config.mailfrom' );
                    else:
                        $para = explode(',', $params->get('para'));
                        foreach($para as $email){
                            $mail->AddAddress($email);
                        }
                    endif;
                    if($params->get('cc') != ''){
                        $cc = explode(',', $params->get('cc'));
                        foreach($cc as $email){
                            $mail->addCC($email);
                        }
                    }
                    if($params->get('cco') != ''){
                        $cc = explode(',', $params->get('cco'));
                        foreach($cc as $email){
                            $mail->addBCC($email);
                        }
                    }
                    //$mail->ClearReplyTos();
                    //$mail->setSender($input->get('email', '', 'string'));
                    $mail->setSubject($params->get('subject'));
                    $mail->addAttachment($dest);
                    $body = "Nome: {$input->get('nome', '', 'string')}
Email: {$input->get('email', '', 'string')}
Telefone: {$input->get('telefone', '', 'string')}";
                    $mail->setBody($body);
                    if($mail->Send()){
                        die('Enviado com sucesso');
                    }else{
                        die('Falha ao enviar o email');
                    }
                }
            }else{
                die('Extensão não permitida');
            }
        }

    }
}
?>
