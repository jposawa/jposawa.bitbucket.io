<?php defined('_JEXEC') or die; ?>
<script type="text/javascript">    
    jQuery(document).ready(function(){   
        jQuery('#enviar').click(function(){     
            email = jQuery('#email').val();
            mensagem = 'Preencher ';
            count = 0;
            if(jQuery('#nome').val() == ''){
                mensagem = mensagem + 'Nome';
                count = 1
            }
            if(email.search(/^\w+((-\w+)|(\.\w+))*\@\w+((\.|-)\w+)*\.\w+$/) == -1){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }                
                mensagem = mensagem +  'E-mail';
                count = 1                
            }
            if(jQuery('#msg').val() == ''){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }                
                mensagem = mensagem +   'Mensagem';
                count = 1                
            }       
            
            if(mensagem != 'Preencher '){
                jQuery.msg({ 
                    content : mensagem
                });                         
                return false;
            }
            
        })
        jQuery('#formContato').ajaxForm({
            success:       showResponse
        });      
    });   
    function showResponse(response){
        jQuery('#formContato').clearForm();
    }
</script> 
 
<div class="caixa_faleconosco">
	
	<h2>Contato</h2>
	
    <form class="caixa_formulario" action="index.php?option=com_contato&task=enviar" method="post" id="formContato">                  	
		
		<div class="fl">
			
			<div class="caixa_campo">
				
				<label for="nome">Nome *:</label>
	        	
	        	<div class="caixa_input"><input type="text" value="" id="nome" name="nome" /><div></div></div>
			
			</div>
			
			<div class="caixa_campo">
			
	        	<label for="email">E-mail *:</label>
	        
	        	<div class="caixa_input"><input type="text" value="" id="email" name="email" /><div></div></div>
			
			</div>
	
			<div class="caixa_campo">
			
	        	<label for="telefone">Telefone:</label>
	        
	        	<div class="caixa_input"><input type="text" value="" id="telefone" name="telefone" maxlength="8" /><div></div></div>
			
			</div>
	        
        </div>
        
        <div class="caixa_campo">
        
        	<label for="mensagem">Mensagem *:</label>
        	
        	<div class="caixa_textarea"><textarea rows="4" id="mensagem" name="mensagem"></textarea><div></div></div>
        
        </div>
        
        <input type="submit" id="enviar" value="Enviar" class="txtIndent" />               
        
        <input type="hidden" name="para" value='<?php echo $this->params->get('para'); ?>' />
        
	</form>
    
    <div class="caixa_enderecocontatos">
    	
    	<?php echo $this->mapa->value; ?>
    	    
        <?php if ($this->contato->nome != ''): ?>              
        
            <?php echo $this->contato->value; ?>
        
        <?php endif; ?>
        
    </div>    

</div>