<?php defined('_JEXEC') or die;
$input = JFactory::getApplication()->input;?>
<script type="text/javascript">    
    jQuery(document).ready(function(){   
        jQuery('#enviar').click(function(){     
            email = jQuery('#email').val();
            mensagem = 'Preencher ';
            count = 0;
            if(jQuery('#nome').val() == ''){
                mensagem = mensagem + 'Nome';
                count = 1
            }
            if(email.search(/^\w+((-\w+)|(\.\w+))*\@\w+((\.|-)\w+)*\.\w+$/) == -1){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }                
                mensagem = mensagem +  'E-mail';
                count = 1                
            }
            if(jQuery('#msg').val() == ''){
                if (count == 1){
                    mensagem = mensagem + ', ';
                }                
                mensagem = mensagem +   'Telefone';
                count = 1                
            }       
            
            if(mensagem != 'Preencher '){
                jQuery.msg({ 
                    content : mensagem
                });                         
                return false;
            }
            
        })
        jQuery('#formContato').ajaxForm({
            success:       showResponse
        });      
    });   
    function showResponseTrabalhe(response){
        if(response == 'Enviado com sucesso'){
            jQuery('#formContato').clearForm();
        }
        alert(response)
    }
</script> 
 
<div class="caixa_faleconosco">
	
	<h2>Trabalhe conosco</h2>
	
    <form class="caixa_formulario" action="index.php?option=com_contato&task=enviarTrabalhe&Itemid=<?php echo $input->get('Itemid'); ?>" method="post" id="formContato">
		
		<div class="fl">
			
			<div class="caixa_campo">
				
				<label for="nome">Nome *:</label>
	        	
	        	<div class="caixa_input"><input type="text" value="" id="nome" name="nome" /><div></div></div>
			
			</div>
			
			<div class="caixa_campo">
			
	        	<label for="email">E-mail *:</label>
	        
	        	<div class="caixa_input"><input type="text" value="" id="email" name="email" /><div></div></div>
			
			</div>
	
			<div class="caixa_campo">
			
	        	<label for="telefone">Telefone *:</label>
	        
	        	<div class="caixa_input"><input type="text" value="" id="telefone" name="telefone" maxlength="8" /><div></div></div>
			
			</div>
	        
        </div>
        
        <div class="caixa_campo">
        
        	<label for="file">Curriculum *:</label>
        	
        	<div class="caixa_textarea"><input id="file" type="file" name="file" /><div></div></div>
            <span>Somente pdf,doc,docx</span>
        
        </div>
        
        <input type="submit" id="enviar" value="Enviar" class="txtIndent" />               
	</form>
</div>