<?php

defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class ClientesController extends JController {
    public function display(){
        $db = JFactory::getDbo();
        $app		= JFactory::getApplication();
        $params		= $app->getParams();
        $catid          = $params->get('catid');
        $query = 'SELECT * FROM #__banners WHERE catid = '.$catid.' AND state = 1 ORDER BY ordering';
        $db->setQuery($query);
        $result = $db->loadObjectList();
        $erro = $db->getErrorMsg();
        if($erro){
            die($erro);
        }
        $view = $this->getView('clientes', 'html'); 
        $view->assignRef('list', $result);
        $view->display();
    }
}

?>
