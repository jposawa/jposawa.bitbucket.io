<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class ClientesViewClientes extends JView {
    public function renderEmailbtn(){
		require_once(JPATH_SITE . '/components/com_mailto/helpers/mailto.php');
		$uri	= JURI::getInstance();
		$base	= $uri->toString(array('scheme', 'host', 'port'));
		$template = JFactory::getApplication()->getTemplate();
		$link	= $base.'/index.php?option=com_clientes';
		$url	= 'index.php?option=com_mailto&tmpl=component&template='.$template.'&link='.MailToHelper::addLink($link);

		$status = 'width=400,height=350,menubar=yes,resizable=yes';
                $text = '&#160;'.JText::_('JGLOBAL_EMAIL');
		$attribs['title']	= JText::_('JGLOBAL_EMAIL');
                $attribs['id']       = 'fancymail';                
		$attribs['onclick']     = "return false;";
                $script = "<script>jQuery(document).ready(function(){
                            jQuery('#fancymail').fancybox({
                                            'autoScale'     	: false,      
                                            'type'              : 'iframe'
                                       });});</script>";

		$output = $script.JHtml::_('link',JRoute::_($url), $text, $attribs);
		return $output;
    }
}

?>
