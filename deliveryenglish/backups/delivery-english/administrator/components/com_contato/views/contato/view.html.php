<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.view');
class ContatoViewContato extends JView
{               
    public function display(){
        $doc = JFactory::getDocument();
        $doc->addScript(JURI::base().'components/com_contato/js/jquery.js');
        JToolBarHelper::title(JText::_('Contato'), 'contact.png');
        JToolBarHelper::apply();        
        parent::display();
    }
}
?>
